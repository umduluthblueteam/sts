@Author: Saketh
================

RESULTS AND ANALYSIS:
========================

SubTask 1 (ENGLISH-STS):
========================
Baseline Approach:
==================
	In order to provide a simple word overlap baseline, first tokenize the input
sentences splitting on white spaces, and then represent each sentence as a 
vector in the multidimensional token space. Each dimension has 1 if the token
is present in the sentence, 0 otherwise. Vector similarity is computed using the
cosine similarity metric.

Our Approach:
=============
	We implemented the Semantic Textual Similarity system, by using a
comprehensive tool called monolingual aligner[1]. It first aligns the words from
two sentences. After computing the list of similar words, we calculate the
proportions of aligned content words for each sentence. Then, we do a harmonic
mean of both proportions to get a final similarity.

Results:
========
=================================================================================|
|Input           |Baseline Average Pearson       |Our Average Pearson Correlation|
|                |                Correlation    |	                             | |================================================================================|
|answers-students| 0.52282                             | 0.56789                 |
|answers-forum   | -0.35420                            | 0.24960                 |
|***images       | 0.72911                             | 0.68561                 |
|headlines       | 0.16888                             | 0.17249                 |
|***belief       | 0.89480                             | 0.82550                 |
|================================================================================|

Our analysis on SubTask 1 results:
==================================
-We ran this subtask using our SubTask 3, but the results were not better,
than our phase one accuracy. So, we planned to keep our approach the same as in
the first phase.
-The trial data is very small (around 5 sentences per input file)
-We manually compared the results of baseline and our approach with the gold
standard
-For every sentence in the input, our computed accuracy is very close
to the gold standard. The absolute difference between our accuracy and the
gold accuracy is smaller when compared to that of the baseline.
-So the results might not be a very good estimate of our accuracy.
-We strongly feel that our approach yields better accuracy when run on test
data.
-The results of the test data are included with in our package
-The accuracy can be computed once the gold standard is released, by running
the scoring program.
-We guess the negative correlation is due to a very bad performace of
baseline.


Subtask-2 (SPANISH STS):
========================
Baseline Approach:
=================
Baseline algorith used in implementing the SubTask2 (Semantic Textual similarity 
for spanish) is Cosine similarity. The appraoch is similar to approach used in 
SubTask 1.

Our Approach & Analysis:
========================
	 In contrast to the translators used in implementing SubTask2 using 
our algorithm, Baseline algorithm  uses only GoSlate API which is used 
for converting the spanish sentences into english. Our approach for implementing 
SubTask2 is  primarily done using two translators namely, Bing Translator API and 
Google Translate API. Unlike Goslate API, Google Translate API gives out multiple 
possible translations for a given spanish sentence. We then use the approach used 
in Subtask1 for computing the similarity scores for two sentencs. Baseline 
approach do not consider all possible translations and it only considers the best 
possible translation which may not be always correct. The main reason for 
low accuracy of Baseline Algorithm is that it leaves out many possible translations 
during the process which is given out by Google Translate API, which ensures that
our approach has better accuracy than Baseline approach.


There is a difference in the GoSlate API and Google Translate API. GoSlate API
gives out the best possible translations and Google translate API gives out all
possible translations of the chunks for the input sentence.


End of Phase 1:

|=================================================================================|
|Input   |Baseline Average Pearson Correlation   |Our Average Pearson Correlation |
|=================================================================================|
|li65    | 0.64588                               | 0.85742                        |
|=================================================================================|

End of Phase 2:

|=================================================================================|
|Input   |Baseline Average Pearson Correlation   |Our Average Pearson Correlation |
|=================================================================================|
|li65    | 0.59243                               | 0.86783                        |
|=================================================================================|

We can see there is a difference between the scores obtained with Baseline approach
and our approach. Our approach certainly gives a better accuracy.

SubTask 3 (Interpretable STS):
=============================
Baseline Approach:
==================
Baseline approach also has 5 stages. Stages 1,2,5 are same as in our approach.
In stage-2 chunks from source-translation sentence pair are aligned only if
they are equal.
In stage-3 chunks are assigned the most frequent alignment type (EQUI) found
in the traning data.
In stage-4 chunks are assigned the average of all alignment scores (4.23) in
training data. 

Our Approach:
=============
As mentioned in section V.6.8 of README.txt systems implementing 
SubTask3 have to process two kinds of inputs:
1. Raw english sentences
2. Chunked english sentences

I.1. Stage-1
-----------
If the input given is raw english sentences they are chunked before 
processing. If the input is chunked english sentences those chunks are read into
the program .

In this stage both file containg source and translation sentences/chunks are
processed.

I.2. Stage-2
------------
In this stage chunks from each source sentence and is aligned with chunks 
from corresponding translation sentence. Monolingual word aligner mentioned
in section III.1. in README.txt is used for finding word alignments of 
source and translation sentences. For each word alignment pair found the
their corresponding chunks are aligned. This information is encoded in a 
datastrcuture called Word Alignment Data Structure (WADS).  

WADS (Word Alignment Data Structures)
+++++++++++++++++++++++++++++++++++++++  
The contents of the WA file(section V.6.B in README.txt) are internally
represented as a datastructure called "Word Alignment Data Structures (WADS)".

Note:
=====
WADS is coded in python hence python's basic datastructures are used in the
description below.

WADS is a list of dictionaries each for a source-translation sentence pair.
Each dictionary encapsulates following alignment information for 
the sentence pair:
    
================================================================================
        dictionary keys           ||              dictionary value
================================================================================
        id                        ||  Unique identifier for the sentence pair   
                                  ||
        status                    ||  Currently empty
                                  ||
        source_sentence           ||  Source sentence
                                  ||  (first sentence in the input pair)
                                  ||
        translation_sentence      ||  Translation sentence     
                                  ||  (second sentence in the input pair)
                                  ||
        source_wordlist           ||  Numbered  word list of source sentence
                                  ||
        translation_worlist       ||  Numbered word list of translation 
                                  ||  sentence 
                                  ||
        alignments                ||  List of dictionaries one for each 
                                  ||  chunk pair. This dictionary contents are 
                                  ||  explained in below table.
================================================================================
                 Table: Word Alignment Data Structures(WADS)


================================================================================
        dictionary keys           ||             dictionary value
================================================================================
        source_chunk_word_indices ||  Indices of the words in the chunk from  
                                  ||  source sentence
                                  ||
  translation_chunk_word_indices  ||  Indices of the words in the chunk from  
                                  ||  translation sentence
                                  ||
                                  ||
        type                      ||  Type of the alignment (EQUI, OPPO, SPE1, 
                                  || SPE2, SIMI, REL, NOALI, ALIC)
                                  ||
        score                     ||  Score for the chunk alignment 
                                  ||
        source_chunk              ||  chunk from the source sentence
                                  ||
        translation_chunk         ||  chunk from the translation sentence
================================================================================
                 Table: Word Alignment Data Structures(WADS)


Following is a rough pseudo code for WADS 
list(
        dictionary(
            'id':
            'status':
            'source_sentence':
            'translation_sentence':
            'source_wordlist':
            'translation_worlist':
            'alignments': list(
                            'alignment': dictionary(
                                'source_chunk_word_indices':
                                'translation_chunk_word_indices':
                                'type':
                                'score':
                                'source_chunk':
                                'translation_chunk':
                            )
                        )
        )
    )


I.3. stage-3
-------------
The chunk alignments from the previous stage are labeled with type of alignment
in this stage. Alignments definde for this task are already described
(V.5. in README.txt). 
We have trained a Radial Bound Function Support Vector Classifier using the 
training data provided by the task organizers. 
POS tags and chunk similarity are used as features for predicting the type of 
alignment.
Explination of features used and classifier implementation can be found 
at "src/TrainChunkFeatures.py".   

I.4. stage-4
-------------
In this stage score are assigned to chunks based on the type of alignment. 
Average scores were calculated for each alignment type and they are used for 
scoring each alignment. The average scores are as follows:
        
        'EQUI'  :   "5",
        'SPE1'  :   "3.75",
        'SPE2'  :   "3.55",
        'ALIC'  :   'NIL',
        'NOALI' :   "0",
        'SIMI'  :   "2.94",
        'REL'   :   "2.82",
        'OPPO'  :   "4",

I.5. stage-5
-------------
stages 2, 3, 4 fill different parts of WADS and WADS for each source-translation
sentence pair as serialized as a WA file in this stage. The serializer
implementation is at src/WAFileHandler.py

Results:
========
The results of this pilot task were very encouraging. They are below:

BASELINE HEADLINES RAW ON TEST DATA
=====================================

F1 Ali     0.3421
F1 Type    0.3363
F1 Score   0.2890
F1 Typ+Sco 0.2868

BASELINE HEADLINES CHUNKED ON TEST DATA
======================================
F1 Ali     0.4207
F1 Type    0.4207
F1 Score   0.3559
F1 Typ+Sco 0.3559



HEADLINES RAW ON TEST DATA
=====================================
Before POS features
F1 Ali     0.7445
F1 Type    0.4517
F1 Score   0.6528
F1 Typ+Sco 0.5060

After POS
F1 Ali     0.7445
F1 Type    0.4517
F1 Score   0.6508
F1 Typ+Sco 0.5262

After removing lengths
F1 Ali     0.7445
F1 Type    0.4608
F1 Score   0.6510
F1 Typ+Sco 0.5399

After adding similarity feature
F1 Ali     0.7445
F1 Type    0.4656
F1 Score   0.6521
F1 Typ+Sco 0.5441

HEADLINES CHUNKED ON TEST DATA
==============================
F1 Ali     0.8600
F1 Type    0.6594
F1 Score   0.7921
F1 Typ+Sco 0.6933




BASELINE IMAGES RAW ON TEST DATA
=====================================
F1 Ali     0.4949
F1 Type    0.4854
F1 Score   0.4178
F1 Typ+Sco 0.4106

BASELINE IMAGES CHUNKED ON TEST DATA
====================================

F1 Ali     0.4773
F1 Type    0.4773
F1 Score   0.4038
F1 Typ+Sco 0.4038


IMAGES RAW ON TEST DATA
=====================================
Before POS features
F1 Ali     0.8291
F1 Type    0.5973
F1 Score   0.7647
F1 Typ+Sco 0.6628

After POS
F1 Ali     0.8291
F1 Type    0.5650
F1 Score   0.7549
F1 Typ+Sco 0.6368

After removing lengths
F1 Ali     0.8291
F1 Type    0.6024
F1 Score   0.7730
F1 Typ+Sco 0.6915

After adding similarity
F1 Ali     0.8291
F1 Type    0.6096
F1 Score   0.7728
F1 Typ+Sco 0.6898

IMAGES CHUNKED ON TEST DATA
=====================================
F1 Ali     0.9179
F1 Type    0.6659
F1 Score   0.8381
F1 Typ+Sco 0.7605



Our Analysis:
=============
-The baseline accuracy is very less when compared to our accuracy.
-The headlines dataset looked quite skewed and hence the alignment accuracy was
vey less.
-But for images dataset the results are very good.
-We experimented on various feature combinations and all the results were
included above.
-The lengths seemed to be a good feature initially but later we realized that
it overshadowed other features, which is not intended.Hence we removed it from
our list.
-The POS tag feature gave as a boost to our results.
-After adding the chunk similarity feature, it was even better.
-We hope to end up having a good accuracy for the test data too.

REFERENCES:
===========
1. ("Sultan, Md Arafat, Steven Bethard, and Tamara Sumner. 
"Back to Basics for Monolingual Alignment: Exploiting Word Similarity and
 Contextual Evidence.")