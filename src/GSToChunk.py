'''
@author: saketh
File Description: This module takes the golden chunked file as input and
returns the list of chunks for each sentence in the file

Output Format:
Format: 
List<.
     .
     List_item1<<Sentence>, <Chunk_list>, <Word_list>>,
     List_item2<<Sentence>, <Chunk_list>, <Word_list>>, 
     List_item3<<Sentence>, <Chunk_list>, <Word_list>>,
     ...>

Eg:
[.
.
.
['Syria peace dashed as deadline passes',
  [[['No_Tag'], ['Syria', 'NNP'], ['peace', 'NN']],
   [['No_Tag'], ['dashed', 'VBD']],
   [['No_Tag'], ['as', 'IN'], ['deadline', 'NN'], ['passes', 'NNS']]],
  ['Syria', 'peace', 'dashed', 'as', 'deadline', 'passes']],
 ['Syria blames rebels for Houla massacre of over 100',
  [[['No_Tag'], ['Syria', 'NNP']],
   [['No_Tag'], ['blames', 'NNS']],
   [['No_Tag'], ['rebels', 'NNS']],
   [['No_Tag'], ['for', 'IN'], ['Houla', 'NNP'], ['massacre', 'NN']],
   [['No_Tag'], ['of', 'IN'], ['over', 'IN'], ['100', 'CD']]],
  ['Syria',
   'blames',
   'rebels',
   'for',
   'Houla',
   'massacre',
   'of',
   'over',
   '100']],
  .
  .
  .]
'''
#For matching the regular expression pattern
import re
#For debugging purposes
import pprint
import nltk
#Instantiate a pprinter object
import unicodedata
from nltk.corpus.reader import chunked
pp = pprint.PrettyPrinter()

#This module converts the file into a list of list of chunks
def GSToChunk(fileName):
    #This is the main list of sentences that will be returned at the end
    #Open the filename
    with open(fileName, 'r') as fp:
        #Read it
        fp = fp.read()
        #Split it with new line character
        fp = fp.decode('iso-8859-2')
        fp = unicodedata.normalize('NFKD', fp).encode('ascii', 'ignore')
        sents = fp.split("\n")
    
    #Loop through each sentence in the input file
    sentencesList = []
    
    for sent in sents:
        #Chunk the sentence only if the length is > 0
        if len(sent) > 0:
            #Pattern for finding the chunks
            patt = '(\[.*?\])'
            #Holds the chunks that are extracted
            chunkList = []
            sentChunkList = []
            #Searches for the pattern in the input sentence
            matchObject = re.findall(patt, sent)
            #For every match/chunk
            list_tagChunks_sent = []
            for match in matchObject:
                #Strip off braces and extra spaces
                match = match[2:-2].strip()
                #Tokenize the chunks using NLTK 
                tokenChunks = nltk.word_tokenize(match)
                #Tag the tokenized chunks
                tagChunks = nltk.pos_tag(tokenChunks)
                list_tagChunks = []
                #The chunk tag is given as No-Tag and is the same for all
                #chunks
                list_tagChunks.append(['No_Tag'])
                #Add it to the list of chunks
                [list_tagChunks.append(list(tags)) for tags in tagChunks]
                #Add the chunks to the list of chunks of sentences
                list_tagChunks_sent.append(list_tagChunks)
                
            #These lists will contain just the text but not any POS Tags
            chunks = []
            words = []
            #Extract chunk text and words from the list
            for newChunk in list_tagChunks_sent:
                wordList = [x[0] for x in newChunk[1:]]
                words = words + wordList
                chunk = ' '.join(wordList)
                chunks.append(chunk)
            
            #Generate the input sentence from the chunk list
            sentence = ' '.join(chunks)
            #The input sentence is added at index 0
            sentChunkList.append(sentence)
            #The chunk list is added at index 1
            sentChunkList.append(list_tagChunks_sent)
            #The word list is added at index 2
            sentChunkList.append(words)
            #Add this list to the main list that will be returned
            sentencesList.append(sentChunkList)
    #For debugging purposes
    #pp.pprint(sentencesList)
    #Return the list of lists of chunks
    return sentencesList

#GSToChunk('../Inputs/SubTask3/train/STSint.input.headlines.sent2.chunk.txt')
