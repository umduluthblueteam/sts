'''
@author: saketh

File Description: This module contains the routines that are useful to convert
the Opennlp chunked input to the chunkList. Another interesting feature of this
module is, it applies the trained chunking rules to merge few chunks.
For eg, PP + NP = PNP
As the Opennlp chunker doesnt give PNP, we had to do it manually. It also misses
few chunking rules, so we trained and learnt few rules from training data.
We then apply those rules, which increased the chunking accuracy.


This module takes in a file name and spits a list of sentences in a chunked
list format 

Output format:
[.
.
.
 ['People ride mopeds in an urban setting .',
  [[['B-NP'], ['People', 'NNS']],
   [['B-VP'], ['ride', 'VBP']],
   [['B-NP'], ['mopeds', 'NNS']],
   [['B-PPB-NP'],
    ['in', 'IN'],
    ['an', 'DT'],
    ['urban', 'JJ'],
    ['setting', 'NN']],
   [['O'], ['.', '.']]],
  ['People', 'ride', 'mopeds', 'in', 'an', 'urban', 'setting', '.']],
 ['Tan domestic cat under black tarp .',
  [[['B-NP'], ['Tan', 'NNP'], ['domestic', 'JJ'], ['cat', 'NN']],
   [['B-PPB-NP'], ['under', 'IN'], ['black', 'JJ'], ['tarp', 'NN']],
   [['O'], ['.', '.']]],
  ['Tan', 'domestic', 'cat', 'under', 'black', 'tarp', '.']],
.
.
.
]
'''

#Imports Pretty Printer for better debugging
import pprint
pp = pprint.PrettyPrinter()

#Begin of module
def OpennlpToChunks(fileName):
    #Open the filename
    with open (fileName, "r") as myfile:
        data=myfile.read()
    #Split the file using new line character
    sentences = data.split('\n')
    #The main list that will be returned at the end of the module
    sentenceChunks = []
    #For each sentence in the iput file
    for str in sentences:
        #Only if the line contains something in it
        if len(str)>0:
            #Strip off useless spaces
            str = str.strip()
            #A list that is specific to each line in the input file
            #A list of these lists will correspond to sentenceChunks list
            sentChunkList = []
            #Split accordingly
            chunks = str.split("|||")
            chunks.pop(0)
            #This list is for holding the chunks
            chunkList = []
            #Seperate the word from its POS tag and add it to the chunk list
            for chunk in chunks:
                words = chunk.split("****")
                tags = [word.split("@@") for word in words]
                chunkList.append(tags)

            #These are few rules that are found to have a significant effect
            #on the chunking accuracy. However only rule 2 and rule 3 are used
            #for increasing the overall system accuracy                
            rule1 = ['B-PP','B-NP','B-PP','B-NP']
            rule2 = ['B-PP','B-NP']
            rule3 = ['B-VP','B-PRT']
            rule4 = ['B-PPB-NP','B-PPB-NP']
            rule5 = ['B-NP','O','B-NP']
            rule6 = ['B-VP','B-ADVP']
            rule7 = ['B-VP','B-PPB-NP','O']
            rule8 = ['N-NP','O']
            
            #List of rules
            rules = []
            #rules.append(rule1)
            rules.append(rule2)
            rules.append(rule3)
            #rules.append(rule4)
            #rules.append(rule5)
            rules.append(rule6)
            #rules.append(rule7)
            #rules.append(rule8)
            
            #This list has all the newly merged chunks after applying the learnt
            #rules
            newChunkList = applyAllRules(chunkList, rules)
            
            #This list just has the words text but not any tags
            #This is used to generate the original sentence back
            chunks = []
            words = []
            #Process each chunk to extract the text in it and add it to chunks
            for newChunk in newChunkList:
                wordList = [x[0] for x in newChunk[1:]]
                words = words + wordList
                chunk = ' '.join(wordList)
                chunks.append(chunk)
            
            #Join all the chunks to form a sentence
            sentence = ' '.join(chunks)
            #The sentence is added to the sentChunkList
            sentChunkList.append(sentence)
            #And so is newChunkList
            sentChunkList.append(newChunkList)
            #Add the wordlist tho sentChunkList
            sentChunkList.append(words)
            #This list is added to the main sentenceChunks
            sentenceChunks.append(sentChunkList)
    
    #For debugging purposes
    #pp.pprint(sentenceChunks)
    #Return the main list of sentences in the above mentioned format
    return sentenceChunks
        

#This module checks if a rule can be applied to a sentence
#This is done by checking if the rule list is a part of sentence tag list
#If it finds a match it returns all the possible start indices in the
#sentence, which can be later used for applying the rules
#fullist corresponds to sentence taglist
#sublist corresponds to rule list
def sublistExists(fulllist, sublist):
    #Holds the starting indices of matchings
    matched = []
    #Used for iterating the loop
    i = 0
    #Loop till the last possible index such that the below condition
    #satisfies
    while i < (len(fulllist)-len(sublist)+1):
        #If a match is found
        if sublist == fulllist[i:i+len(sublist)]:
            #Then add the index to the matches list
            matched.append(i)
            #Increment i by the length of the rule list
            i = i+len(sublist)
        #Else increment i by 1
        else:
            i = i+1
    #Return the list of indices. It will return an empty list if no 
    #match is found
    return matched    

#This module applies the rules given a sentence list and a rule list
#It returns new chunks after merging them according to the rules
def applyAllRules(ckList, rules):
    #Apply each rule from the rule list
    for rule in rules:
        #It holds the chunk tags for each chunk (NP, PP, VP)
        chunkTagList = []
        for chunk in ckList:
            #The first element in first list of every chunk list contains the tag
            chunkTagList.append(chunk[0][0])
        
        #This list holds the possible matches and their positions
        indexlist1 = sublistExists(chunkTagList, rule)
        #If the list is not empty, them apply the rules
        if indexlist1:
            #For each match in the matchings, merge
            for tempIndex in indexlist1:
                #New tag is named by joining the rule
                ckList[tempIndex][0][0] = ''.join(rule)
                #Merge the contents of the old chunks to form a new chunk
                for i in range(1, len(rule)):
                    ckList[tempIndex] = ckList[tempIndex] + ckList[tempIndex+i][1:]   
            #Once a new chunk is formed, delete old chunks starting from end
            #to avoid indexing problem
            for tempIndex in reversed(indexlist1):
                #Deleting old chunks
                for i in range(1,len(rule)):
                    del ckList[tempIndex+1]
            #Free the list after using it
            del chunkTagList[:]
    
    #Return the new chunk list
    return ckList
