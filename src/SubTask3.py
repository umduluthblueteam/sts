'''
AUTHOR: VISWANADH 

* Brief description of SubTask3 is available in section V.5. in README.txt
* Input and output specification of SubTask3 is available in section V.6.B 
  in README.txt

This module contains the implementation for SubTask3 (Interpretable STS -
http://alt.qcri.org/semeval2015/task2/index.php?id=proba). External scripts 
are required to interface the following function:

SubTask3(source_file, translation_file, output_file_name, 
                                          chunked = False, baseline=False) 

Functions implementing different stages
----------------------------------------
* stage-1 : 
    chunkFile(file) for raw english sentences input
    processChunkFile(file) for chunked english sentences input

* stage-2 : 
    align_chunks(source_chunks, translation_chunks, baseline)

* stage-3 :
    predictChunkTypes(aligned_chunks, baseline=False)

* stage-4 :
    predictScores(aligned_chunks, baseline=False)

# stage-5 :
    src/WAFileHandler.py/write_wa_file(
            aligned_chunks_with_types_and_scores, output_file)
'''

###Modules from python standard library
import os
import sys
from itertools import izip, count
import tempfile
import time
import unicodedata
from multiprocessing import Pool

#third party modules
os.sys.path.insert(1, '../monolingual-word-aligner')
import numpy as np
from sklearn.externals import joblib
from aligner import *
import jsonrpc 

#Modules written by us
import WAFileHandler
from utils import *
from TrainChunkFeatures import *
from OpennlpToChunks import OpennlpToChunks
from GSToChunk import GSToChunk


#initialize logger 
pl = pretty_logger(__name__)
pretty_critical = pl.pretty_critical
pretty_debug = pl.pretty_debug
pretty_info = pl.pretty_info

#total number of lines just for debugging
total_lines = 0

def SubTask3(source_file, translation_file, output_file_name, 
                                              chunked = False, baseline=False):
    """
    Subtask3 interface function. This is called by main script.
    :param source_file - File containing source sentences
    :param translation_file - File containing translation sentences
    :param output_file_name - path for the output WA file
    :param chunked - True if the input is chuked otherwise False,
                       False by default
    :param baseline - True if baseline scores algorithm is to be used for
                       processing the input
    
    The output of this function will be written to output_file_name in 
    'word alignment (WA) format'
    """
    source_chunks, translation_chunks=([],[])
    if not chunked:
        #if the input files do not contain chunked sentences, chunk them
        source_chunks=chunkFile(source_file)
        translation_chunks=chunkFile(translation_file)
    else:
        #if the input files contain chunked sentences, process those chunks 
        source_chunks = processChunkFile(source_file)
        translation_chunks = processChunkFile(translation_file)
    # for debugging 
    global total_lines
    total_lines = len(source_chunks)
    #align the chunks from previous step
    aligned_chunks = align_chunks(source_chunks, translation_chunks, baseline)
    #predict the type of alignments from previous step
    aligned_chunks_with_types = predictChunkTypes(aligned_chunks, baseline)
    #predict the scores for the alignments from previous step
    aligned_chunks_with_types_and_scores = predictScores(
                                            aligned_chunks_with_types, baseline)
    #write the output to the output file
    WAFileHandler.write_wa_file(
                        aligned_chunks_with_types_and_scores, output_file_name)

def chunkFile(file):
    """
        Function to chunk sentences in the given file.
        The output of this function is of the following format
        list(chunk_ds) where,
          list(sentence, chunks_list, words_in_sentence))
          chunks_list = list(chunk_tag_and_words)
          chunk_tag_and_words = list(chunk_tag, (words_in_chunk, POS_tag)...)
        Example of output
        [ .
          .
          .
           ['People ride mopeds in an urban setting .',
            [[['B-NP'], ['People', 'NNS']],
             [['B-VP'], ['ride', 'VBP']],
             [['B-NP'], ['mopeds', 'NNS']],
             [['B-PPB-NP'],
              ['in', 'IN'],
              ['an', 'DT'],
              ['urban', 'JJ'],
              ['setting', 'NN']],
             [['O'], ['.', '.']]],
            ['People', 'ride', 'mopeds', 'in', 'an', 'urban', 'setting', '.']],
           ['Tan domestic cat under black tarp .',
            [[['B-NP'], ['Tan', 'NNP'], ['domestic', 'JJ'], ['cat', 'NN']],
             [['B-PPB-NP'], ['under', 'IN'], ['black', 'JJ'], ['tarp', 'NN']],
             [['O'], ['.', '.']]],
            ['Tan', 'domestic', 'cat', 'under', 'black', 'tarp', '.']],
          .
          .
          .
        ]
    """
    #TODO: compile the java file from the shell script and make sure the class 
    #javac -cp "../Opennlp/Jars/*" OpennlpTools.java
    
    temp_file = tempfile.NamedTemporaryFile(delete=True)
    temp_chunk_file = temp_file.name
    #command to run on the java chunker program using OpenNLP API.
    #It expects an input file of sentences and output file to write the output
    #to a temporary file which is then fed to a custom tailored function which 
    #returns the chunks along with other information like the sentence from 
    #which the chunks are made and words in the sentence  
    # 
    CHUNKING_COMMAND = 'java -cp "../Opennlp/Jars/*:." OpennlpTools {} {}'.format(
                                                            file, temp_chunk_file)
    if os.system(CHUNKING_COMMAND)!=0:
        pretty_critical("Chunker unavailable")
        exit()
    chunks = OpennlpToChunks(temp_chunk_file)
    return chunks

def processChunkFile(file):
    """
    Function to read the chunked input.
    The output of this function is of the following format
        list(chunk_ds) where,
        list(sentence, chunks_list, words_in_sentence))
        chunks_list = list(chunk_tag_and_words)
        chunk_tag_and_words = list(chunk_tag, (words_in_chunk, POS_tag)...)
        Example of output
        [ .
          .
          .
           ['People ride mopeds in an urban setting .',
            [[['B-NP'], ['People', 'NNS']],
             [['B-VP'], ['ride', 'VBP']],
             [['B-NP'], ['mopeds', 'NNS']],
             [['B-PPB-NP'],
              ['in', 'IN'],
              ['an', 'DT'],
              ['urban', 'JJ'],
              ['setting', 'NN']],
             [['O'], ['.', '.']]],
            ['People', 'ride', 'mopeds', 'in', 'an', 'urban', 'setting', '.']],
           ['Tan domestic cat under black tarp .',
            [[['B-NP'], ['Tan', 'NNP'], ['domestic', 'JJ'], ['cat', 'NN']],
             [['B-PPB-NP'], ['under', 'IN'], ['black', 'JJ'], ['tarp', 'NN']],
             [['O'], ['.', '.']]],
            ['Tan', 'domestic', 'cat', 'under', 'black', 'tarp', '.']],
          .
          .
          .
        ]
    """
    chunks = GSToChunk(file)
    return chunks
    
def mapChunksToWordNums(chunks, wordlist):
    '''
    Function to find Indices of words of each chunk in a list of chunks
    params: chunks - List of chunks
    params: wordnumber - list of words - the indices of words in chunks are
                                         found based on ordering of words in 
                                         this list 
    return - list of input chunks along with word indices for each chunk
              list(
                      list(chunk, word_indices)
                    )
    '''
    wordnumber = 1
    chunks_with_indices = list()
    #The algorithm is based on the assumption that chunks are in the same order
    #as they appear in the origional sentence
    #Algorithm:
    #1)Try to match a chunk with a sequence of words in the word list
    #2)If match found, pair the chunk with the indices of words that 
    #  matched the chunk
    #3)Reapeat the above steps for the next chunk and try to match the words
    #  starting with the next unmatched word from previous step
    for chunk in chunks:
        temp_str = wordlist[wordnumber-1]
        temp_indices = "" + str(wordnumber)
        while chunk != temp_str:
            wordnumber = wordnumber + 1
            temp_indices = temp_indices  + " " + str(wordnumber) 
            temp_str = temp_str + " " + wordlist[wordnumber-1]
        wordnumber = wordnumber+1
        chunks_with_indices.append((chunk, temp_indices))
    return chunks_with_indices


def parallel(chunk_ds_list, baseline):    
    '''
    Helper function to compute the alignment of chunks of a sentence pair
    :param - chunk_ds_list - list(linenum, source_chunk_ds, translation_chunk_ds)
    where source_chunk_ds, translation_chunk_ds are of the format

    list(sentence, chunks_list, words_in_sentence))
    chunks_list = list(chunk_tag_and_words)
    chunk_tag_and_words = list(chunk_tag, (words_in_chunk, POS_tag)...)
    
    :param - baseline - If true align the chunks for baseline

    Output is WADS for sentence pair with id = linenum
    '''

    start_time = time.time()
    def getChunkWords(chunk_tag_and_words):
        '''
          helper function to seperate and retrieve chunks from a list of the 
          following format

          list(chunk_tag, (words_in_chunk, POS_tag)...)

        '''
        chunks = []
        #Process each chunk to extract the text in it and add it to chunks
        for newChunk in chunk_tag_and_words[1]:
            wordList = [x[0] for x in newChunk[1:]]
            chunk = ' '.join(wordList)
            chunks.append(chunk)
        # pretty_debug(chunks)    
        return chunks

    linenum, source_chunk_ds, translation_chunk_ds = chunk_ds_list
    
    #debuggin puposes
    global total_lines
    percent =  linenum/float(total_lines)
    percent *= 100
    sys.stdout.write("\rAligning chunks: %.1f%% " % percent)
    sys.stdout.flush()
    pretty_debug("Processed line: %04d in %.2fs" % (linenum + 1 ,time.time() - start_time))
    #debuggin puposes

    #get source sentence
    left = source_chunk_ds[0]
    #get translation sentence
    right = translation_chunk_ds[0]
    
    done = False
    #coreNLP is closing down connections intermittently hence keep trying until
    #it gives output
    while not done:
        try:
            #get word alignments from monolingual word aligner
            word_alignments = align(left, right)
            done = True
        except jsonrpc.RPCTransportError as rp:
            pass
    
    # pretty_debug(right)

    #create a WADS to hold alignments information
    alignment_ds = dict()
    alignment_ds['id'] = linenum + 1
    alignment_ds['status']=""
    alignment_ds['source_sentence']=source_chunk_ds[0]
    alignment_ds['translation_sentence']=translation_chunk_ds[0]
    alignment_ds['source_wordlist'] = source_chunk_ds[2]
    alignment_ds['translation_worlist'] = translation_chunk_ds[2]
    alignment_ds['alignments'] = list()
    
    #seperate chunks from other information in the chunk data structures
    source_chunk_words = getChunkWords(source_chunk_ds)
    translation_chunk_words = getChunkWords(translation_chunk_ds)
    # pretty_debug(left)
    # pretty_debug(right)
    # pretty_debug("words: " , word_alignments[2])
    # pretty_debug("words: ", word_alignments[3])
    # pretty_debug("Source chunk ds:", source_chunk_ds)
    # pretty_debug("Translation chunk ds:",translation_chunk_ds)
    # pretty_debug(source_chunk_words)
    # pretty_debug(translation_chunk_words)

    #map the chunks with their word indices
    sent1chunks = mapChunksToWordNums(source_chunk_words, source_chunk_ds[2])
    sent2chunks = mapChunksToWordNums(translation_chunk_words, translation_chunk_ds[2])
    
    if baseline:
        #
        #   For baseline align chunks with same text
        #
        i = len(sent1chunks) -1
        while i >=0:
            aligned_chunks=dict()
            j = len(sent2chunks) -1
            while j>=0:
                source_chunk = sent1chunks[i]
                translation_chunk = sent2chunks[j]
                j-=1
                if source_chunk[0] == translation_chunk[0]:
                    aligned_chunks['source_chunk'] = source_chunk[0]
                    aligned_chunks['source_chunk_word_indices'] = source_chunk[1]
                    aligned_chunks['translation_chunk']=translation_chunk[0]
                    aligned_chunks['translation_chunk_word_indices'] = translation_chunk[1]
                    alignment_ds['alignments'].append(aligned_chunks)
                    sent1chunks.remove(source_chunk)
                    sent2chunks.remove(translation_chunk)
                    break
            i-=1    

    else:
        #
        # For words aligned by monolingual word alinger align the corresponding
        # chunks
    
        for (word_in_sen1, word_in_sen2) in word_alignments[1]:
            if not word_in_sen1 in stopwords.words('english') and word_in_sen1 not in punctuations:
                aligned_chunks=dict()
                aligned_chunks['source_chunk'] = "-not aligned-"
                aligned_chunks['source_chunk_word_indices'] = "0"
                for each_chunk in sent1chunks:
                    if each_chunk[0].find(word_in_sen1)!=-1:
                        aligned_chunks['source_chunk'] = each_chunk[0]
                        aligned_chunks['source_chunk_word_indices'] = each_chunk[1]
                        sent1chunks.remove(each_chunk)
                        break

                aligned_chunks['translation_chunk']="-not aligned-"
                aligned_chunks['translation_chunk_word_indices'] = "0"
                for each_chunk in sent2chunks:
                    if (each_chunk[0].find(word_in_sen2)) != -1:
                        aligned_chunks['translation_chunk'] = each_chunk[0]
                        aligned_chunks['translation_chunk_word_indices'] = each_chunk[1]
                        sent2chunks.remove(each_chunk)
                        break

                 
                #We are getting an empty chunk because of initializations 
                #I have done in lines 152 and 159
                #So just remove the empty chunk 
                if not (aligned_chunks['source_chunk']=="-not aligned-" and aligned_chunks['translation_chunk']=="-not aligned-"):
                    alignment_ds['alignments'].append(aligned_chunks) 
    # for chunks in source sentence which are unaligned to any chunk in 
    # translation sentence align them to an imaginary chunk "-not aligned-" with
    #word inidices "0"
    for each_chunk in sent1chunks:
        aligned_chunks=dict()
        aligned_chunks['source_chunk'] = each_chunk[0]
        aligned_chunks['translation_chunk']="-not aligned-"
        aligned_chunks['source_chunk_word_indices'] = each_chunk[1]
        aligned_chunks['translation_chunk_word_indices']  = "0"
        alignment_ds['alignments'].append(aligned_chunks) 

    # for chunks in translation sentence which are unaligned to any chunk in 
    # source sentence align them to an imaginary chunk "-not aligned-" with
    #word inidices "0"
    for each_chunk in sent2chunks:
        aligned_chunks=dict()
        aligned_chunks['source_chunk'] = "-not aligned-"
        aligned_chunks['translation_chunk'] = each_chunk[0]
        aligned_chunks['source_chunk_word_indices'] = "0"
        aligned_chunks['translation_chunk_word_indices'] = each_chunk[1]
        alignment_ds['alignments'].append(aligned_chunks) 

    return alignment_ds

def align_chunks(source_chunks, translation_chunks, baseline):
    """Function to align chunks given by processChunkFile(file) or
    chunkFile(file) for source sentences and translation sentences.
    It uses monolingual-word-aligner
    ("Sultan, Md Arafat, Steven Bethard, and Tamara Sumner. 
    "Back to Basics for Monolingual Alignment: Exploiting Word Similarity and Contextual Evidence.")
    which is available at "https://github.com/ma-sultan/monolingual-word-aligner".
    The monolingual-word-aligner is used to align words in a pair of sentences. This alignment is
    used to align the chunks.
    
    The output of this functoin is WADS without score and type for chunk
    alignments
    """

    output_list=list()
    punctuations = ['(','-lrb-','.',',','-','?','!',';','_',':','{','}',
                          '[','/',']','...','"','\'',')', '-rrb-', "\'\'", "``"]
   
    for linenum, source_chunk_ds, translation_chunk_ds in izip(count(), source_chunks, translation_chunks):
        output_list.append((linenum, source_chunk_ds, translation_chunk_ds)) 

    #map each chunk information from source sentences and translation sentences
    # to the funtion parallel
    # we can run the function parallel in seperate threads

    output = [parallel(temp, baseline) for temp in output_list]
    print "\rAligning chunks: 100.0%"
    return output

            
        
def predictChunkTypes(aligned_chunks, baseline=False):
    """
        Function to predict the type of alignment for the aligned chunks for
        each sentence pair computed by
        `align_chunks(source_chunks, translation_chunks)`.
        
        :param - aligned_chunks - list of WADS without scores and types for 
                                  chunk alignments for all sentences
        :params - baseline - If true type prediction is done for basline i.e, 
                            most frequent alignment type (EQUI) in the 
                            training data. 

        This function returns list of WADS without chunk alignment scores for 
        all sentences
    """
    #load the "Radial Bound function Support Vector Classifier"
    rbf = joblib.load('Models/rbf_svc.pkl')
    targetLabels = {0 : 'EQUI',
                    1 : 'OPPO',
                    2 : 'SPE1',
                    3 : 'SPE2',
                    4 : 'SIMI',
                    5 : 'REL',
                    6 : 'ALIC',
                    7 : 'NOALI'}

    for chunk_ds_list in aligned_chunks:
        for chunk in chunk_ds_list['alignments']:
            sChunk = chunk['source_chunk']
            tChunk = chunk['translation_chunk']
            #convert the chunk pair into featutre vector
            rowFeature = getChunkFeatures(sChunk, tChunk)
            #predict the type for the chunk pair
            predicted = rbf.predict(rowFeature)
            if not baseline:
                #if not baseline predict the scores using RBF SVC classifier
                chunk['type'] = targetLabels[int(predicted[0])]
            else:
                #if baseline make type EQUI (most frequent chunk alignment
                #                             type in the training data)
                chunk['type'] = targetLabels[0] #Always EQUI for baseline
        #debuggin puposes
        global total_lines
        percent =  int(chunk_ds_list['id'])/float(total_lines)
        percent *= 100
        sys.stdout.write("\rPredicting chunk types: %.1f%% " % percent)
        sys.stdout.flush()
        #debuggin puposes
    print "\rPredicting chunks types: 100.0%"
    return aligned_chunks

def predictScores(aligned_chunks, baseline=False):
    """
        Function to predict scores for aligned chunks. Scores range from 0-5. 
        The scores assigned for tags are average scores for each chunk types
        calculated using training data.
        :params - aligned_chunks - list of WADS without scores and type for chunk 
                                 alignments for all sentence pairs
        :params - baseline - If true scoring is done for basline i.e, average
                             of all scores in the training data. 
        This function returns list of WADS for all sentence pairs
    """
    #Avg scores found for each alignment type in the training data
    scores = {
        'EQUI'  :   "5",
        'SPE1'  :   "3.75",
        'SPE2'  :   "3.55",
        'ALIC'  :   'NIL',
        'NOALI' :   "0",
        'SIMI'  :   "2.94",
        'REL'   :   "2.82",
        'OPPO'  :   "4",
    }

    #For baseline Avg score for all alignment types in the training data is
    #considered. 
    baseline_scores = {
        'EQUI'  :   "4.23",
       # 'NOALI' :   "0",
    }

    for chunk_ds in aligned_chunks:
        for chunk in chunk_ds['alignments']:
            if not baseline:
                chunk['score'] = scores[chunk['type']]
            else:
                chunk['score'] = baseline_scores[chunk['type']]
        #debuggin puposes
        global total_lines
        percent =  int(chunk_ds['id'])/float(total_lines)
        percent *= 100
        sys.stdout.write("\rScoring chunk alignments: %.1f%% " % percent)
        sys.stdout.flush()
        #debuggin puposes
    print "\rScoring chunk alignments: 100.0%"
    return aligned_chunks

#code to test the module. please ignore this
if __name__=='__main__':
    baseline = False
    chunked = False
    if (len(sys.argv)>5):
        if(sys.argv[4] == 'chunked'):
            chunked=True
        if(sys.argv[5] == 'baseline'):
            baseline=True
    logging.basicConfig(level=logging.INFO)
    SubTask3(sys.argv[1], sys.argv[2], sys.argv[3], chunked=chunked, baseline=baseline)

    