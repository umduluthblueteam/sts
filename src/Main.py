'''
Team BLUE STS 
Viswanadh: 
I was mainly responsible for implementing the Subtask3 algorithm.
I have put a lot of time on how to implement the subtask-3. Most of my time 
was spent on reading papers then an overview of how to develop the system was
started during the second week of November. I colloborated with the team to explain
on how to tackle the problem at hand and finally I managed to implement an algorith
which performed significantly better than the baseline.


Saketh Ram:
I colloborated well with all the team members to conduct meetings and how to 
implement the second phase of our task. I managed to read several paper and discussed
ideas with the team during team meetings. I tried tho analyze why  our subtask-1
approach gave a result less than the score obtained from baseline algorithm for two
of the Inputs. The reasoning was described in the baselines textfiles. Finally 
we managed to implement the system which performed better than baseline in every case.

Charan Raj:
I was mainly respsonsible for implementing the SubTask2 (Semantic 
textual similarity for spanish). For the First phase the approach was to the 
system using the GoSlate API whihc converts sentences from one language to another.
During the second phase, I have mainly tried to find the way to increase the 
accuracy of our algorithm. I have searched for other useful approaches for 
converting the sentnece into english. Then, I came across Bing Translator which 
lead to a little increase in accuracy than GoSlate API. Then, I have further
searched other best options. I came across a paper which found out multiple 
translations of the chunk in the sentence. I have contacted the paper authors 
requesting the Jar file( Google Translate API) required to get the multiple 
translations. Using this measure lead to further incrase in accuracy. Further 
contributed to the ideas in improving the accuracy for subtask3. Contributed the 
devolopment of the Main.py. 
'''
'''
#Authors: Viswanadh

#Description: This is the main file of our system, which calls all the subtasks internally.
#This file will be called
#by the runit.sh script. 
'''

#os package is used for handling the file directory paths
import os, errno, sys
from SubTask1 import SubTask1_implement
from SubTask1Baseline import SubTask1_Baseline_Implement
from SubTask2 import SubTask2_implement
from SubTask2Baseline import SubTask2_Baseline_Implement
from SubTask3 import SubTask3
import logging
import subprocess

#Author: Vish
#This function returns the list of files located in a given <path> which is passed as an argument
def list_files(path):
    #Initialize the files list to empty
    files = []
    #Iterate each file in the directory to expand to its complete path
    for name in os.listdir(path):
        #Joins the parent path with the filename intelligently and checks if it is a file
        if os.path.isfile(os.path.join(path, name)):
            #If it is a file, it is added to the list of files
            files.append(name)
    #Return the list of files in the given directory
    return files

#Initialize various variables to hold the paths of input files

''' 

######################################################## SubTask1
How the module works:

Below is the module which implements the SubTask 1. SubTask1 returns the 
similarity score score of two sentences given in english. Initially the 
sentneces are provided in atext file where the sentneces to be comapared
are seperated by a tab space. Each pair of sentneces are seperated by a 
new line. There are several files in the input folder, each classified 
as a seperate task. This program goes throught each and every file in 
the input folder and computes the similarity score using our alogirthm 
and the baseline algorithm and write the scores onto a seperate file. 
These files are stored in a seperate output folder described by the name 
of its subtask.For example every input task file is ultimatley written 
into its output folder. Finally, Pearson coefficient is computed for the
results obtained for Baseline and our algorithm. Pearson coefficient
values are written onto a seperate file.

Detailed description on how this module works in mentioned in the
SubTask1.py

All the files used in the Subtask Functionality

    Input Folder : Inputs/SubTask1
    input Task File : STS.input.answers-forum.txt
    Gold standard File : STS.gs.input.answers-forum.txt
    Output FOlder : Outputs/Subtask1
    Output File : answers-forum.res
    Output Baseline result file : answers-forum.baseline.res
    Pearson coefficient file : answers-forum.psn

    These are the input files with their corresponding output files.
'''

def  SubTask1_run(input_dir, output_dir, scoring_program):
    try:
        os.makedirs(output_dir)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(output_dir):
            pass
        else: raise
    
    
    count=0
    # tasknames is the set initialized here to empty to store the names of the  
    # all the names of tasks 
    tasknames=set()

    # to loop through every input file in the directory
    for inputfilename in list_files(input_dir):
        # we retrieve the name of the task in the input directory and append it
        # to the list
        tasknames.add(inputfilename.split('.')[2])
    
     # this variable is used as a name of the input file in the Input
        # folder of that particular subtask
    source_file_template = 'STS.input.{}.txt'
    
    # print len(tasknames)
    # To run the Subtask on every input file in the directory,
    # tasknames has names of all tasks in the input folder
    print "\nProcessing SubTask 1:"
    for task in tasknames:
        print "\nFor input file: " + task
        # we add the task name and file position to the source_file_template
        # it is similarly done to the ouput_file and output_baselinefile 
        source_file = input_dir + "/" + source_file_template.format(task)
        output_file = output_dir + "/" +task + ".res"
        output_baselinefile = output_dir + "/" + task  +"."+ "baseline" + ".res"

        print "Running baseline..."
        # this step calls the Baseline algorithm for implementing the SubTask2
        SubTask1_Baseline_Implement(source_file,output_baselinefile)

        print "\nRunning our implementation..."
        # this step calls our algorithm for implementing the SubTask1
        SubTask1_implement(source_file,output_file)

        '''
        This module is responsible for the evaluation of the subtask.
        In this module we run the correlation program against the scores
        generated by the system along with the gold standard scores. This 
        module is evaluated only if the gold standard scores are provided along 
        with the test data.

        '''
        print "\nRunning scoring program for input: " + task
        PEARSON_COMMAND = "perl " + scoring_program
        COMMAND_Baseline = PEARSON_COMMAND + " " + input_dir + "/" + 'STS.gs.' + task + ".txt " + output_dir + "/" + task + ".baseline.res " + " > " + output_dir + "/"+ task+ ".psn"
        if os.system(COMMAND_Baseline)!=0:
            print "Scoring program/Gold standard not found"
            pass

        COMMAND = PEARSON_COMMAND + " " + input_dir+ "/" + 'STS.gs.' + task + ".txt " + output_dir + "/"+ task + ".res " + " >> " + output_dir + "/" + task+ ".psn"
        #Checks for successful execution of command
        if os.system(COMMAND)!=0:
            print "Scoring program/Gold standard not found"
            pass
    print "Results for SubTask 1 saved to: " + output_dir



'''
SubTask2
############################################# 


How the module works:

Below is the module which implements the SubTask 2. SubTask2 returns the 
similarity score score of two sentences given in spanish. Initially the 
sentneces are provided in a text file where the sentneces to be comapared
are seperated by a tab space. Each pair of sentneces are seperated by a 
new line. There are several files in the input folder, each classified 
as a seperate task. This program goes throught each and every file in 
the input folder and computes the similarity score using our alogirthm 
and the baseline algorithm and write the scores onto a seperate file. 
These files are stored in a seperate output folder given  by the name 
of its subtask.For example every input task file is ultimatley written 
into its output folder. Finally, Pearson coefficient is computed for the
results obtained for Baseline and our algorithm. Pearson coefficient
values are written onto a seperate file.

For processing SubTask2 we have two module, where the sentences are
converted into English sentences using Bing Translator and in the
second module we translate the spanish sentence using GoogleTranslateAPI
which gives out all probabable sentences for the given spanish sentence 
while translating. We then take "most probable sentences"  from the given 
translated result. We finally take an average of the result formed by
Bing Translator API and GoogleTranslateAPI

We divide the SubTask2.py into two modules, first module works on 
basis on Bing Translator result and second module works on basis
of GoogleTranslateAPI.

Here is how the module works in Input and output processing
-> First Module( Inputfile.txt ) and returns the similarity
score using our alogirthm

-> Second Module (SpanishFinal.txt)
 Inputfile.txt ------>  InputFormany.txt ------->  SpanishFinal.txt
                     (GoslateTranslate.java)       (InputParsing.py)


Detailed description on how this module works in mentioned in the
SubTask2.py


  All the files used in the Subtask Functionality

    Input Folder : Inputs/SubTask2
    input Task File : STS.input.answers-forum.txt
    Gold standard File : STS.gs.input.answers-forum.txt
    Output Folder : Outputs/Subtask2
    Output File : answers-forum.res
    Output Baseline result file : answers-forum.baseline.res
    Pearson coefficient file : answers-forum.psn
    InputFormany.txt: File generated by the GoslateTranslate.java, 
    this file is stored in /tmp
    SpanishFinal.txt : Generated by the InputParsing.py, 
    which is given as input for second Module in SubTask2.py,
    this is stored in /tmp

    These are the input files with their corresponding output files.

'''

# This is the preprocessing step before computing the 
# similarity score in the second module of the Subtask2
# This step is responsible for generating the intermediate files
# required to implement the second module of SubTask2.py as mentioned 
# above

def Google_processing(parsefile,count):
    
    # This varaible is used to check the number of task in the input folder
    # so as to create the corresponding processing files for that input  
    count = str(count)
    # java program is run here to generate the file which is given as an input to
    # Input Parsing.py
    Java_command= 'java -cp ../GoogleTranslate/Jars/GoogleTranslateAPI.jar:.  GoslateTranslate'+ ' ' + parsefile+ ' '+ count 
    #print Java_command
    
    # Below lines are used to check if the command was successfullt executed or not,
    # if not we exit
    if os.system(Java_command)!=0:
        print "Error in running GoslateTranslate java program"
        return
    else:
    # If the java program is successfully executed , then we run InputParsing.py
    # which generates the input for second module in the Subtask2.py
    # JsonParse is the command to run python program InputParsing.py
        JsonParse = 'python InputParsing.py' +' '+count 
        #print JsonParse
        if os.system(JsonParse)!=0:
			print "Error in Json parsing"
			return



def  SubTask2_run(input_dir, output_dir, scoring_program):
    
    try:
        os.makedirs(output_dir)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(output_dir):
            pass
        else: raise
    
    # this variable is used to check the number of task in the given input folder
    count=0
    # this is the set used to store the names of task in the input folder
    # i.e the type of data on which the system is running
    tasknames=set()

    # to loop through every input file in the directory
    for inputfilename in list_files(input_dir):
         # we retrieve the name of the task in the input directory and append it
         # to the list
        tasknames.add(inputfilename.split('.')[2])
    
     # this variable is used as a name of the input file in the Input
        # folder of that particular subtask
    source_file_template = 'STS.input.{}.txt'
    
    #print tasknames
     
    print "\nProcessing SubTask 2:"
    # To run the Subtask on every input file in the directory
    # tasknames has names of all tasks in the input folder
    for task in tasknames:
        
        # prints the name of the task on which system is running
        print "\nFor input file: " + task
        count=count+1

        # we add the task name and file position to the source_file_template
        # it is similarly done to the ouput_file and output_baselinefile 
        source_file = input_dir + "/" + source_file_template.format(task)
        output_file = output_dir + "/" +task + ".res"
        output_baselinefile = output_dir + "/" + task  + "."+"baseline" + ".res"

        print "Google preprocessing..."
        # the preprocessing required to run second module of the Subtask2
        Google_processing(source_file,count)

        print "Running baseline..."
        # this step calls the Baseline algorithm for implementing the SubTask2
        SubTask2_Baseline_Implement(source_file,output_baselinefile)
        
        print "\nRunning our implementation..."
        # this step calls our algorithm for implementing the SubTask2 
        SubTask2_implement(source_file,output_file,count)

        '''
        This module is responsible for the evaluation of the subtask.
        In this module we run the correlation program against the scores
        generated by the system along with the gold standard scores. This 
        module is evaluated only if the gold standard scores are provided along 
        with the test data.

        '''
        print "\nRunning scoring program for input: " + task
        PEARSON_COMMAND = "perl " + scoring_program
        # this command runs the person correlation against the scores generated by
        # the baseline algorithm
        COMMAND_Baseline = PEARSON_COMMAND + " " + input_dir + "/" + 'STS.gs.' + task + ".txt " + output_dir + "/" + task + ".baseline.res " + " > " + output_dir + "/"+ task+ ".psn"
        
        # checks the successful execution of the command
        if os.system(COMMAND_Baseline)!=0: 
            print "Scoring program not found"
            pass
        # this command runs the person correlation against the scores generated by
        # our algorithm    
        COMMAND = PEARSON_COMMAND + " " + input_dir+ "/" + 'STS.gs.' + task + ".txt " + output_dir + "/"+ task + ".res " + " >> " + output_dir + "/" + task+ ".psn"
       
        #Checks for successful execution of command
        if os.system(COMMAND)!=0:
            print "Scoring program not found"
            pass
    print "Results for SubTask 2 saved to: " + output_dir


##########################SubTask3######################### 

def  SubTask3_run(input_dir, output_dir, validation_program, scoring_program):

    def run(source_file_template, translation_file_template, output_file_template, chunked, baseline):
      '''
        helper function to run various configurations of SubTask3
      '''
      for task in task_names:
        result_str = task
        result_str += " with chunked input" if chunked else " with raw sentences input"
        result_str += " using baseline prediction" if baseline else " using SVM prediction"
        
        print "proccessing: ", result_str
        source_file = input_dir + "/" + source_file_template.format(task)
        translation_file = input_dir + "/" + translation_file_template.format(task)
        #output file
        output_file = output_dir + "/" +output_file_template.format(task)
        #run subtask 3 for a task
        SubTask3(source_file, translation_file, output_file, chunked, baseline)

        print "saved results to: ", output_file, "\n"

    def score(validation_program, scoring_program, output_file, gold_file):
        '''
            Helper function to evaluate the well formedness and F1 score of results generated by subtask-3
        '''
        #run validation command
        try:
          results = subprocess.check_output([validation_program, output_file])
          print results
        except subprocess.CalledProcessError as err:
          print "wellformed error"
          return

        #run scoring command and write print the results
        try:
          results = subprocess.check_output([scoring_program, gold_file, output_file])
          print results
        except CalledProcessError as err:
          print " scoring err"
          return 

    #create output directory if it does not exist
    try:
        os.makedirs(output_dir)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(output_dir):
            pass
        else: raise
          
    # find the input tasks that need to be executed i.e, headlines, images etc
    # This relies on SemEval naming convention
    task_names = set()
    for inputfilename in list_files(input_dir):
        splits = inputfilename.split('.')
        if len(splits)>3:
          task_names.add(splits[2])
    
    #naming conventions for input files containing raw english sentences
    raw_source_file_template="STSint.input.{}.sent1.txt"
    raw_translation_file_template = "STSint.input.{}.sent2.txt"
    raw_output_file_template = "{}.wa"
    
    #run baseline for raw english sentences
    raw_baseline_output_file_template = "{}.baseline.wa"
    run(raw_source_file_template, raw_translation_file_template, raw_baseline_output_file_template, chunked=False, baseline=True)

    #run SVM prediction
    run(raw_source_file_template, raw_translation_file_template, raw_output_file_template, chunked=False, baseline=False)

    

    #naming conventions for input files containing chunked english sentences
    chunked_source_file_template="STSint.input.{}.sent1.chunk.txt"
    chunked_translation_file_template = "STSint.input.{}.sent2.chunk.txt"
    chunked_output_file_template = "{}.chunk.wa"
    
    #run baseline for chunked english sentences
    chunked_baseline_output_file_template = "{}.chunk.baseline.wa"
    run(chunked_source_file_template, chunked_translation_file_template, chunked_baseline_output_file_template, chunked=True, baseline=True)
    
    #run SVM prediction
    run(chunked_source_file_template, chunked_translation_file_template, chunked_output_file_template, chunked=True, baseline=False)

    

    raw_gold_file_template = "STSint.gs.{}.wa"
    chunked_gold_file_template = "STSint.gs.{}.chunk.wa"
    validation_program = os.getcwd()[:-3]+validation_program
    scoring_program = os.getcwd()[:-3]+scoring_program
    print validation_program
    print scoring_program
    #run scoring
    for task in task_names:
      raw_gold_file = input_dir + "/" + raw_gold_file_template.format(task)
      chunked_gold_file = input_dir + "/" + chunked_gold_file_template.format(task)
      result_str = task

      #scoring for raw output using baseline prediction
      print result_str+" using baseline prediction"
      print"=============================="
      output_file = output_dir + "/" + raw_baseline_output_file_template.format(task)
      score(validation_program, scoring_program, output_file, raw_gold_file)


      print result_str+" using SVM prediction"
      print"=============================="
      #scoring for raw output using SVM prediction
      output_file = output_dir + "/" + raw_output_file_template.format(task)
      score(validation_program, scoring_program, output_file, raw_gold_file)

      #scoring for chunked input using baseline prediction
      print result_str+" chunked input using baseline prediction"
      print"=============================="
      output_file = output_dir + "/" + chunked_baseline_output_file_template.format(task)
      score(validation_program, scoring_program, output_file, chunked_gold_file)


      #scoring for chunked input using SVM prediction
      print result_str+" chunked input using SVM prediction"
      print"=============================="
      output_file = output_dir + "/" + chunked_output_file_template.format(task)
      score(validation_program, scoring_program, output_file, chunked_gold_file)



if __name__=='__main__':
  
    logging.basicConfig(level=logging.INFO)
    SUBTASK_1_INPUT_DIRECTORY_PATH   = sys.argv[1]
    SUBTASK_2_INPUT_DIRECTORY_PATH   = sys.argv[2]
    SUBTASK_3_INPUT_DIRECTORY_PATH   = sys.argv[3]
    SUBTASK_1_OUTPUT_DIRECTORY_PATH  = sys.argv[4]
    SUBTASK_2_OUTPUT_DIRECTORY_PATH  = sys.argv[5]
    SUBTASK_3_OUTPUT_DIRECTORY_PATH  = sys.argv[6]
    SCORING_PROGRAM_SUBTASK_1_2      = sys.argv[7]
    VALIDATION_PROGRAM_SUBTASK_3     = sys.argv[8]
    SCORING_PROGRAM_SUBTASK_3        = sys.argv[9]
    #Run subtask 1
    SubTask1_run( SUBTASK_1_INPUT_DIRECTORY_PATH,
                  SUBTASK_1_OUTPUT_DIRECTORY_PATH,
                  SCORING_PROGRAM_SUBTASK_1_2)
    #Run subtask 2
    SubTask2_run( SUBTASK_2_INPUT_DIRECTORY_PATH,
                  SUBTASK_2_OUTPUT_DIRECTORY_PATH,
                  SCORING_PROGRAM_SUBTASK_1_2)
    #Run subtask 3
    SubTask3_run( SUBTASK_3_INPUT_DIRECTORY_PATH,
                  SUBTASK_3_OUTPUT_DIRECTORY_PATH,
                  VALIDATION_PROGRAM_SUBTASK_3,
                  SCORING_PROGRAM_SUBTASK_3)