#author :Charan Raj

'''
This file is responsible for calculating the similarites for two sentences.
This file is used in both Subtask1.py and Subtask2.py. Score is calculated in
the range 0-1 for the two sentences passed as arguments. 

Here sim is the similarity score which is returned 
'''


from aligner import *
import math

# This is the function which is responsible for calculating the similarities.
# It takes in the arguments
# of alignmnets which is returned from the align function which is present in
# the aligner file.

def scoring(alignments):
      contentCount1, contentCount2, alignCount1, alignCount2 = 0, 0, 0, 0
      for word in alignments[1]:
            # this if statements checks if it finds any stop words from the list
            # of  similar words and ignore the stop words and punctuation
            # Stop words are being removed since they remove the efficiency in
            # calculating the similarity score of the sentences
            if word[0] not in stopwords + punctuations + ['\'s', '\'d', '\'ll']:
                  alignCount1 += 1;
            
      # This for loop goes through the alignments[1] which contains
      # several lists( each list is a pair of similar words) and removes
      # the stop word in the second word in the lists
      for word in alignments[1]:
            if word[1] not in stopwords + punctuations + ['\'s', '\'d', '\'ll']:
                  alignCount2 += 1;
            
      # Alignments 2 is a list which has words from first sentence and
      # removes stop words an punctuation from it            
      # it also identifies the content words from the list of all words 
      for word in alignments[2]:
            if word not in stopwords + punctuations + ['\'s', '\'d', '\'ll']:
                  contentCount1 += 1;
            
      # Alignments 3 is a list which has words from second sentence and
      # removes stop words an punctuation from it 
      # it also identifies the content words from the list of all words           
      for word in alignments[3]:
            if word not in stopwords + punctuations + ['\'s', '\'d', '\'ll']:
                  contentCount2 += 1;
              
            
      # The calculations of prop1 and prop2  are done according to the
      # formula mentioned in the paper "Sentence Similarity from Word
      # Alignment" 
      if contentCount1 != 0:
          prop1 = alignCount1 / float(contentCount1)
      else:
          prop1 = 0
      if contentCount2 != 0:
          prop2 = alignCount2 / float(contentCount2)
      else:
          prop2 = 0
            
            
      # This is the formula for calculating the similarity of the two
      # sentences, it is in the range of 0-4 for Spanish Sentences
      if prop1 != 0:
            sim = (2 * prop1 * prop2 ) / (prop1 + prop2)
      else:
            sim = 0
      # To write the output scores of the all the compared sentences into a file
      return sim


''' 
This module calculates the cosine similarity (dot product) given two vectors.
get_cosine function implements the function of computing the similarity scores
of the given sentences. Arguments that are passed into the function include two
vectors which consist of set of tokens computed from the input sentences.
Initially set of common words are computed and then numerator variable is
calculated by computing sum of the products of the corresponding entries of the 
two vectors. Denominator computes the mathematical operation of multiplying the
square of sum1 and sum2.
Citation: http://stackoverflow.com/questions/15173225/how-to-calculate-cosine-
similarity-given-2-sentence-strings-python
'''
def get_cosine(vec1, vec2):
    # This is used to store the set of common words from the two sentences
    intersection = set(vec1.keys()) & set(vec2.keys())
    # numerator is computed by multiplying the corresponding entries from vec1 and vec2
    numerator = sum([vec1[x] * vec2[x] for x in intersection])
    sum1 = sum([vec1[x]**2 for x in vec1.keys()])
    sum2 = sum([vec2[x]**2 for x in vec2.keys()])
    # denominator is computed by multiplying the square of sum1 and sum2
    denominator = math.sqrt(sum1) * math.sqrt(sum2)
    
    if not denominator:
        return 0.0
    else:
        # this function returns a score which is the similarity score for the given sentences
        return float(numerator) / denominator
