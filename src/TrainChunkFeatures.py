'''
Created on Dec 4, 2014

@author: saketh
File Description:
This file contains the modules that are required to train a classifier. We
tried using 3 classifiers in SVC (Support Vector classifier). They are:
-->LinearSVC
-->SVC with RBF Kernel (Radial Basis Function)
-->SVC with Poly Kernel (Polynomial)

The main goal of SubTask 3 is to predict the type labels for aligned chunks.
Thus, the alignment type mainly depends on the pair of aligned chunks. We 
focus on extracting features from the pair of chunks.

Features we tried including:
-->Length of source chunk
-->Length of translation chunk
-->No of nouns in source chunk
-->No of verbs in source chunk
-->No of adjectives in source chunk
-->No of prepositions in source chunk
-->No of nouns in translation chunk
-->No of verbs in translation chunk
-->No of adjectives in translation chunk
-->No of prepositions in translation chunk
-->The path similarity between words of source and translation chunks

The lenth appeared to work fine initially but the POS tags helped in
acheiving more accuracy

The best accuracy was acheived using SVC with RBF Kernel, so we used it for
predicting the chunk labels

'''
#Import all the required packages
#Numpy is used to create feature vectors and target vectors
import numpy as np
import nltk
import unicodedata
from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn

import pprint
#We use sklearn which has a lot of well built functionality for
#machine learning
from sklearn import svm, datasets
from sklearn import cross_validation
from sklearn import metrics
from WAFileHandler import read_wa_file
#This is used to export the trained model
from sklearn.externals import joblib
import sys
#import matplotlib.pyplot as plt

#This can be replaced by a command lie parameter
#goldStandardFile = sys.argv[1]
#Used for debugging purposes
pp = pprint.PrettyPrinter()
#retrieve the dictionary of sentences, given a training file

'''
This module takes in a gold standard file and spits the required
trained models. The gold standard file is the word alignment (.wa) file
which has the alignment type label for each pair of chunks. We calculate
the chunk features for each pair in the gold standard and assign the label
to the target vector to train the classifier.

Once the model is trained, we store the model and export it for future
use. This is done using model persistence of sklearn.
'''
def trainClassifiers(goldStandardFile):
    #Read the gold file and obtain all the sentences as a list of
    #dictionaries
    list_sent = read_wa_file(goldStandardFile)
    #Loop through all the sentences to get the chunks and their features
    '''
    The above dictionary has the following format:
    Eg:
    Dictionary format:
     {'alignments': [{'source_chunk': '-not aligned-',
                      'source_word_indices': '0',
                      'translation_chunk': 'in a bathroom sink',
                      'translation_word_indices': '6 7 8 9',
                      'score': 'NIL',
                      'type': 'NOALI'},
                     {'source_chunk': 'closely',
                      'source_word_indices': '4',
                      'translation_chunk': '-not aligned-',
                      'translation_word_indices': '0',
                      'score': 'NIL',
                      'type': 'NOALI'},
                     {'source_chunk': 'look',
                      'source_word_indices': '3',
                      'translation_chunk': 'looking',
                      'translation_word_indices': '11',
                      'score': '5',
                      'type': 'EQUI'},
                     {'source_chunk': '.',
                      'source_word_indices': '8',
                      'translation_chunk': '-not aligned-',
                      'translation_word_indices': '0',
                      'score': 'NIL',
                      'type': 'NOALI'},
                     {'source_chunk': '-not aligned-',
                      'source_word_indices': '0',
                      'translation_chunk': '.',
                      'translation_word_indices': '14',
                      'score': 'NIL',
                      'type': 'NOALI'},
                     {'source_chunk': 'at the camera',
                      'source_word_indices': '5 6 7',
                      'translation_chunk': 'at the camera',
                      'translation_word_indices': '11 12 13',
                      'score': '5',
                      'type': 'EQUI'},
                     {'source_chunk': 'Tan cows',
                      'source_word_indices': '1 2',
                      'translation_chunk': 'A white and gray cat',
                      'translation_word_indices': '1 2 3 4 5',
                      'score': '2',
                      'type': 'SIMI'}],
      'id': '375',
      'source': 'Tan cows look closely at the camera .',
      'translation': 'A white and gray cat in a bathroom sink looking at the camera .',
      'source': ['Tan', 'cows', 'look', 'closely', 'at', 'the', 'camera', '.'],
      'status': '',
      'translation': ['A',
                      'white',
                      'and',
                      'gray',
                      'cat',
                      'in',
                      'a',
                      'bathroom',
                      'sink',
                      'looking',
                      'at',
                      'the',
                      'camera',
                      '.']}
    '''
    #Representing a feature vector of 2 features. Increase the no of columns
    #when features are increased. Each row represents a sample in the training data
    #Format: [<length of source chunk> <length of translation chunk>]  
    #Format ([5 6]
    #        [7 8])
    #Intialize the feature vector with all zeros
    #It is of shape m x n:
    #where, m is the number of training samples
    #       n is the number of features for each training sample
    featureVector = np.zeros((13,),dtype=np.float)

    #Target Vector contains the target labels/indices for each of the training samples
    #Format ([0 1])
    #It is of the shape: 1 x m i.e., one value for each training sample
    target_Vector = np.array([])
    #Maps a target label to a target index
    target_Vector_names = {'EQUI': 0,
                           'OPPO': 1,
                           'SPE1': 2,
                           'SPE2': 3,
                           'SIMI': 4,
                           'REL': 5,
                           'ALIC': 6,
                           'NOALI': 7}

    #Start looping through the list of dictionaries
    for sentence in list_sent:
        alignments = sentence['alignments'];
        # Feature vector for each pair of chunks
        # Format:
        # [<length of source chunk> <length of translation chunk>]
        #Initialize a row vector
        # rowFeature = np.array([0,0]);
        #Loop through the list of aligned chunks
        for alignment in alignments:
            sChunk = alignment['source_chunk']
            tChunk = alignment['translation_chunk']
            sChunk = sChunk.decode('iso-8859-2')
            sChunk = unicodedata.normalize('NFKD', sChunk).encode('ascii', 'ignore')
            tChunk = tChunk.decode('iso-8859-2')
            tChunk = unicodedata.normalize('NFKD', tChunk).encode('ascii', 'ignore')
            rowFeature = getChunkFeatures(sChunk, tChunk)
            
            #Add the row to the main feature vector
            featureVector = np.vstack([featureVector, rowFeature])
            
            #Retrieve the target label
            target_feature = target_Vector_names[alignment['type']]
            
            #Append the target label to the target vector
            target_Vector = np.append(target_Vector, target_feature)
            
    #Delete the dummy array while initializing the feature Vector
    featureVector = np.delete(featureVector, (0), axis=0)

    #By default the truncated version of the ndarray will be printed
    #To print the whole array, set the printing options as below 
    np.set_printoptions(threshold='nan')
    
    #
    C = 1.0
    #Fit the training data using LinearSVC
    lin = svm.LinearSVC(C=C)
    lin_svc = lin.fit(featureVector, target_Vector)
    joblib.dump(lin, 'Models/lin_svc.pkl')

    #Fit the training data using SVC with RBF Kernel
    rbf = svm.SVC(kernel='rbf', gamma=0.7, C=C)
    rbf_svc = rbf.fit(featureVector, target_Vector)
    joblib.dump(rbf, 'Models/rbf_svc.pkl')

    #Fit the training data using SVC with Poly Kernel
    poly = svm.SVC(kernel='poly', degree=3, C=C)
    poly_svc = poly.fit(featureVector, target_Vector)
    joblib.dump(poly, 'Models/poly_svc.pkl')

    #print "Accuracy for LinearSVC: " + str(lin.score(X_test,y_test))
    print "Accuracy for SVC with RBF: " + str(rbf.score(featureVector,
                                                target_Vector))
    print "Accuracy for SVC with Poly: " + str(poly.score(featureVector,
                                                target_Vector)) 

    #scores_lin = cross_validation.cross_val_score(lin, featureVector,
    #target_Vector, cv=5, n_jobs = -1)
    scores_rbf = cross_validation.cross_val_score(rbf,
                         featureVector, target_Vector, cv=5, n_jobs = -1)
    scores_poly = cross_validation.cross_val_score(poly,
                         featureVector, target_Vector, cv=5, n_jobs = -1)

    #print "K fold Accuracy for LinearSVC: " + str(scores_lin)
    print "K fold Accuracy for SVC with RBF: " + str(scores_rbf)
    print "K fold Accuracy for SVC with Poly: " + str(scores_poly)

'''
This module is used to obtain the feature vector for a pair of chunks
Thie same method is used while training the classifier and while predicting
the label.
It has all the features stored as a row in an nd array
This array will be returned to the calling module
'''
def getChunkFeatures(sChunk, tChunk):
    #rowFeature contains n features. Its shape will be 1 x n
    #current features (11) = [sChunk_length, tChunk_length, source_no_of_Nouns,
    #source_no_of_Verbs, source_no_of_Adjectives, source_no_of_Prep,
    #translation_no_of_Nouns, translation_no_of_Verbs, translation_no_of_
    #Adjectives, translation_no_of_Prep]
    
    #Initialize the row feature to zero
    rowFeature = np.zeros((13,),dtype=np.float)
    
    #To enable the lengths feature, we can turn the below 3 lines on
    dictLength = getChunkLengths(sChunk, tChunk)
    rowFeature[0] = dictLength['sl']
    rowFeature[1] = dictLength['tl']
    #rowFeature[0] = 0
    #rowFeature[1] = 0

    #Obtain the POS tags for the chunk pair by using the getPOSCounts module
    dictPOS = getPOSCounts(sChunk, tChunk)
    #Obtain counts using the keys of dictionary
    rowFeature[2] = dictPOS['sn']
    rowFeature[3] = dictPOS['sv']
    rowFeature[4] = dictPOS['sa']
    rowFeature[5] = dictPOS['sp']
    rowFeature[6] = dictPOS['tn']
    rowFeature[7] = dictPOS['tv']
    rowFeature[8] = dictPOS['ta']
    rowFeature[9] = dictPOS['tp']
    
    rowFeature[10] = getChunkSimilarity(sChunk, tChunk)

    rowFeature[11] = getUnigramOverlapCount(sChunk, tChunk)
    #rowFeature[11] = 0

    rowFeature[12] = getBigramOverlapCount(sChunk, tChunk)
    #rowFeature[12] = 0
    #Return the computed feature vector
    return rowFeature

'''
This module returns the length of each chunk in the form of a dictionary
It assignes a 0 length for chunks which dont have any alignment
'''
def getChunkLengths(sChunk, tChunk):
    #Initialize the dictionary to zero lengths
    lengthCount = {'sl':0,
                   'tl':0}
    #If the chunk is aligned to some chunk, then find the length 
    #by splitting with space
    if (sChunk != '-not aligned-'):
        lengthCount['sl'] = len(sChunk.split(" "))
    #Compute the length for translation chunk in the same way
    if (tChunk != '-not aligned-'):
        lengthCount['tl'] = len(tChunk.split(" "))
    
    #Return the dictionarys
    return lengthCount

'''
This module computes the POS tags and their frequencies for both the chunks
We take nouns, verbs, adjectives and prepositions into consideration.
This is because, they form the basis for constructing Noun, Verb and Preposi-
tional phrases (NP,VP and PP)
'''
def getPOSCounts(sChunk, tChunk):
    #Initialize the dictionary to zero counts
    featureCount = {'sn':0,
                    'sv':0,
                    'sa':0,
                    'sp':0,
                    'tn':0,
                    'tv':0,
                    'ta':0,
                    'tp':0}
    #Tag the chunks using NLTK tokenizer
    #We used NLTK instead of Opennlp because NLTK is much more comfortable
    #to used with chunked input data. Also Opennlp doesn't have a very good
    #Python wrapper, so invoking jvm everytime will be a very bad idea
    lTokens = nltk.word_tokenize(sChunk)
    rTokens = nltk.word_tokenize(tChunk)

    #Tag the tokens using NLTK
    lTags = nltk.pos_tag(lTokens)
    rTags = nltk.pos_tag(rTokens)

    #We use the following lists for detecting Nouns, Verbs, Adjectives and
    #Prepositions
    nouns = ['NN','NNS','NNP','NNPS']
    verbs = ['VB','VBD','VBG','VBN','VBP','VBJ']
    adjectives = ['JJ','JJR','JJS']
    prepositions = ['IN']

    #If the chunk is not aligned, it doesn't have any POS tags
    #and hence POS tags are only calculated for aligned chunks
    if (sChunk != '-not aligned-'):
        #For all the words, check the POS tag and increment the
        for lTup in lTags:
            #Check if the POS tag is a noun
            if lTup[1] in nouns:
                featureCount['sn'] = featureCount['sn']+1
            #Check if the POS tag is a verb
            elif lTup[1] in verbs:
                featureCount['sv'] = featureCount['sv']+1
            #Check if the POS tag is an adjective
            elif lTup[1] in adjectives:
                featureCount['sa'] = featureCount['sa']+1
            #Check if the POS tag is a preposition
            elif lTup[1] in prepositions:
                featureCount['sp'] = featureCount['sp']+1

    if (tChunk != '-not aligned-'):
        for rTup in rTags:
            #Check if the POS tag is a noun
            if rTup[1] in nouns:
                featureCount['tn'] = featureCount['tn']+1
            #Check if the POS tag is a verb
            elif rTup[1] in verbs:
                featureCount['tv'] = featureCount['tv']+1
            #Check if the POS tag is an adjective
            elif rTup[1] in adjectives:
                featureCount['ta'] = featureCount['ta']+1
            #Check if the POS tag is a preposition
            elif rTup[1] in prepositions:
                featureCount['tp'] = featureCount['tp']+1
    #Return the POScount dictionary
    return featureCount

'''
This is the moudle that is called from the getWordSimilarity
function. this module is responsible for calculating the 
final similarity score.

'''
def getChunkSimilarity(sChunk,tChunk):

  stops = set(stopwords.words('english'))
  Sent_Words1=[]
  Sent_Words2=[]
  scorelist1=[]
  avg_score = 0 
  
  if sChunk == '-not aligned-' or tChunk == '-not aligned-':
      return avg_score

  for word in sChunk.split(" "):
      if word not in stops:
        Sent_Words1.append(word)

  #print Sent_Words1

  for word in tChunk.split(" "):
      if word not in stops:
        Sent_Words2.append(word)

  #print Sent_Words2

  for word in Sent_Words1:
    for word2 in Sent_Words2:
      score = getWordSimilarity(word,word2)
      scorelist1.append(score)
      avg_score+=score
  if not len(scorelist1) == 0:
      avg_score=avg_score/len(scorelist1)

  return avg_score

'''
This module calculates the similarity given two chunks.
it works by conidering the words in the given chunk.
This module works by computing the path similarity given 
by the word net. we initiallty consider all the synsets 
of a given word and compute the similarity of every word 
given by that sysnset with the word in the other chunk.
this process is done for all the words in the two chunks. 
Score is returned by the getChunkSimilarity function defined 
above. In the process of computing similarity, we first remove 
the stopwords in

'''
# This is the dictionary which reduces the time tin computing 
# the similarity for the word that already occured. It stores 
# the similarity score for the pair of words in the dictionary 
# and if the next time th pair occurs the similarity is returned 
# from the dictionary
wpath_sim_cache={}

def getWordSimilarity(word1,word2):

  pair= (word1,word2)
  if pair in wpath_sim_cache:
    return wpath_sim_cache[pair]

  if word1 == word2:
    wpath_sim_cache[pair]=1
    return 1

  sim1 = wn.synsets(word1)
  sim2 = wn.synsets(word2)
  similarity = max([wa.path_similarity(wb)
            for wa in sim1
            for wb in sim2
            ] + [0.])
  wpath_sim_cache[pair] = similarity
  return similarity

'''
This module calculates the unigram overlap count from two
chunks. It returns the number of overlapped words.
'''
def getUnigramOverlapCount(sChunk, tChunk):
    #If the chunk is not aligned, return zero
    if sChunk == '-not aligned-' or tChunk == '-not aligned-':
      return 0
    sChunkSet = set(sChunk.split(' '))
    tChunkSet = set(tChunk.split(' '))
    #Return the number of overlapped words
    return len(sChunkSet & tChunkSet)

'''
This module calculates the bigram overlap count from two
chunks. It returns the number of overlapped bigrams.
'''
def getBigramOverlapCount(sChunk, tChunk):
    #If the chunk is not aligned, return zero
    if sChunk == '-not aligned-' or tChunk == '-not aligned-':
      return 0
    sChunkList = sChunk.split(' ')
    tChunkList = tChunk.split(' ')
    sChunkBigramSet = set()
    tChunkBigramSet = set()

    #Add consecutive words to corresponding bigram sets
    for i in range(len(sChunkList)-1):
        sChunkBigramSet.add(sChunkList[i] + ' ' + sChunkList[i+1])
    for i in range(len(tChunkList)-1):
        tChunkBigramSet.add(tChunkList[i] + ' ' + tChunkList[i+1])

    #Return the number of overlapped bigram chunks
    return len(sChunkBigramSet & tChunkBigramSet)
    

if __name__=='__main__':

    trainClassifiers(sys.argv[1])