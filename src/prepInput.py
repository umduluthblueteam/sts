'''
Author: Viswanadh
Module to generate train, cross validation and test data randomly for the given dataset
This code is used just for generating random test data for training the classifer.
That is why it is not well documented.
'''
import random
import math
from WAFileHandler import *
import utils
import sys
import logging 
from itertools import izip

pl = utils.pretty_logger(__name__)
pretty_info = pl.pretty_info 
logging.basicConfig(level=logging.INFO)

def prepInput(gs_file, source_chunk_file, translation_chunk_file):
    pretty_info(gs_file)
    #read gold dtandard input
    XMLs=read_wa_file(gs_file)
    total_XMLs=len(XMLs)
    percent_60 = math.floor(total_XMLs * 0.6)
    total_samples = range(0, total_XMLs)
    #compute id's to be included as training sample
    train_samples = set(random.sample(total_samples, int(percent_60)))
    cv_and_test_samples = list(set(total_samples) - train_samples)
    # pretty_info(len(cv_and_test_samples))
    percent_20 = math.floor(len(cv_and_test_samples) * 0.5)
    #compute id's to be included as cross validation sample
    cv_samples = set(random.sample(cv_and_test_samples, int(percent_20)))
    #compute id's to be included as test sample
    test_samples = set(cv_and_test_samples) - cv_samples
    # pretty_info(test_samples.intersection(cv_samples))
    # pretty_info(test_samples.intersection(train_samples))
    # pretty_info(train_samples.intersection(cv_samples))
    pretty_info("cv", len(cv_samples))
    pretty_info("test", len(test_samples))
    #train
    pretty_info("Writing " + str(len(train_samples)) + " samples as train data to "+
                        gs_file+".train")
    temp = [XMLs[sample] for sample in train_samples]
    #Serialize the training samples
    write_wa_file(temp, gs_file+".train")

    #test
    pretty_info("Writing " + str(len(test_samples)) + " gold samples as test data to "+
                        gs_file+".test")
    #serialize the test samples
    temp = [XMLs[sample] for sample in test_samples]
    #rewriting id's 
    for index, temp1 in enumerate(temp):
        temp1['id'] = (index+1) 
    write_wa_file(temp, gs_file+".test")

    pretty_info("Writing " + str(len(test_samples)) + " input senteces as test data to "+
                        gs_file+".source.input.test and "+
                        gs_file+".translate.input.test")
    source_file = open(gs_file+".source.input.test", 'w')
    translation_file = open(gs_file+".translate.input.test", 'w')
    for XML in temp:
        source_file.write(XML['source_sentence'] + "\n")
        translation_file.write(XML['translation_sentence'] + "\n")
    source_file.close()
    translation_file.close()

    chunk_output_source_file = open(gs_file+".source.input.chunk.test", 'w')
    chunk_output_translate_file = open(gs_file+".translate.input.chunk.test", 'w')
    
    # sample = 0
    # print sorted(test_samples)
    # with open(source_chunk_file) as s_c_file , open(translation_chunk_file) as t_c_file:
    #     for s_c, t_c in izip(s_c_file, t_c_file):
    #         if sample in sorted(test_samples):
    #             chunk_output_source_file.write(s_c)
    #             chunk_output_translate_file.write(t_c)
    #         sample = sample + 1
    # chunk_output_source_file.close()
    # chunk_output_translate_file.close()

    #cv
    #serialize the cross validation samples
    pretty_info("Writing " + str(len(cv_samples)) + " samples as cv data to "+
                        gs_file+".cv")
    temp = [XMLs[sample] for sample in cv_samples]
    #rewriting id's
    for index, temp1 in enumerate(temp):
        temp1['id'] = (index+1) 

    write_wa_file(temp, gs_file+".cv")
    pretty_info("Writing " + str(len(test_samples)) + " input senteces as test data to "+
                        gs_file+".source.input.test and "+
                        gs_file+".translate.input.test")
    source_file = open(gs_file+".source.input.cv", 'w')
    translation_file = open(gs_file+".translate.input.cv", 'w')
    for XML in temp:
        source_file.write(XML['source_sentence'] + "\n")
        translation_file.write(XML['translation_sentence'] + "\n")
    source_file.close()
    translation_file.close()

    
if __name__ == '__main__':
    # prepInput('../Inputs/SubTask3/gold/STSint.gs.images.wa',
    #  '../Inputs/SubTask3/train/STSint.input.images.sent1.chunk.txt',
    #  '../Inputs/SubTask3/train/STSint.input.images.sent2.chunk.txt')

    # prepInput('../Inputs/SubTask3/gold/STSint.gs.headlines.wa',
    #  '../Inputs/SubTask3/train/STSint.input.headlines.sent1.chunk.txt',
    #  '../Inputs/SubTask3/train/STSint.input.headlines.sent2.chunk.txt')
    pass