'''
Created on Nov 23, 2014

@author: saketh
File Description: This file contains modules, that are used for
obtaining chunking rules from the training data. More details
included in the module comments.

Eg: Merge - B-PP|B-NP: 430
    Merge - B-VP|B-NP: 150
    Split - B-NP|O|B-VP: 34
'''
from GSToChunk import GSToChunk
from OpennlpToChunks import OpennlpToChunks
import operator

def TrainRulesOpennlp(goldFile, OpennlpFile):
    '''
    In this module,
    We compare the Opennlp chunked output with the gold standard chunks.
    If a gold chunk is formed by merging/splitting two Opennlp chunks,
    we note it as a rule and store it in a dictionary. We keep repeating
    this process, untill all the possible rules are learnt.
    We then sort the rules based on the counts, and use them for forming
    new chunks from the Opennlp output.
    '''
    #Open the files
    goldSentences = GSToChunk(goldFile)
    opennlpSentences = OpennlpToChunks(OpennlpFile)
    #Initialize the dictionary
    rules = {}
    #Iterate through all the sentences from gold and Opennlp
    # sentences
    for list1, list2 in zip(goldSentences, opennlpSentences):
                
        goldChunks = list1
        opennlpChunks = list2[1]
         
        i = 0;
        j = 0;
        #While there are chunks in both the gold chunks and
        #opennlp chunks
        while i < len(goldChunks) and j < len(opennlpChunks):
            #Split the chunks using " "
            goldChunklen = len(goldChunks[i].split(" "))
            opennlpChunkslen = len(opennlpChunks[j])-1
            rule = ""
            #If both the chunks are of same length, then no specific
            #is learnt
            if goldChunklen == opennlpChunkslen:
                #print "same length"
                rule = "No change"
            #If the gold chunk is larger, then add consecutive
            #chunks from opennlp chunks to form the chunk of
            #length equal to gold standard
            elif goldChunklen > opennlpChunkslen:
#                     print goldChunks[i]
#                     print opennlpChunks[j]
#                     print "gold chunk larger, keep adding"
                rule = "Merge: "+ opennlpChunks[j][0][0]
                newChunklen = opennlpChunkslen
                #Loop thill the lengths are equal
                while (goldChunklen != newChunklen) and j+1 < len(opennlpChunks):
#                   # keep adding
                    j = j+1
                    newChunklen = newChunklen + len(opennlpChunks[j])-1
                    #Add the chunk tag to the rule
                    rule = rule + "|"+ opennlpChunks[j][0][0]
                    #print rule
            # If the gold length is smaller, then split the
            # opennlp chunk and learn a rule
            elif goldChunklen < opennlpChunkslen:
                #print "gold chunk smaller, so split opennlp chunk"
#                     rule = "Split"
                tempX = -1;
                rule = "Split: " + opennlpChunks[j][tempX][1]
                newChunklen = goldChunklen
                #Split till the lengths are equal
                while (goldChunklen != newChunklen) and i+1 < len(goldChunks):
                    i = i+1
                    newChunklen = len(goldChunks[i].split(" "))
                    #Add the chunk tag to the rule
                    rule = rule + "|"+ opennlpChunks[j][-1][1]
                    tempX = tempX-1
            #Add the rule to the dictionary if not there
            if rule not in rules:
                    rules[rule] = 1
            #Else, increment the counter
            else:
                    rules[rule] += 1
            i = i+1
            j = j+1
    #sorted rules contains all the rules in sorted order based on the
    #counts
    sorted_rules = sorted(rules.items(), key=operator.itemgetter(1))
    #For printing the scores in decreasing order
    for i in reversed(sorted_rules):
        print i

