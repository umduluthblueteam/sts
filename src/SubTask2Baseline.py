# coding: utf-8

'''
Author: Charan Raj

Task Description:

Given two sentences of text, s1 and s2, the systems participating in this task
should compute how similar s1 and s2 are, returning a similarity score, and an
optional confidence score. The scores should range from 0 to 4, where 4 marks
paraphrases, and 0, sentences that have absolutely no relation.

BASELINE APPROACH:
==================
    In order to provide a simple word overlap baseline, first tokenize the input
sentences splitting on white spaces, and then represent each sentence as a
vector in the multidimensional token space. Each dimension has 1 if the token
is present in the sentence, 0 otherwise. Vector similarity is computed using the
cosine similarity metric.

Program Description:

The program written written below implements the baseline algorithm. It computes
the similarity of the given sentences by implementing the cosine similarity of
the sentences. 

1. Initially name of the file is given as an command line arguments.
2. File consisting of number of Spanish sentences are read, Each line in the
file has two sentences for which similarity is to be evaluated.
3. The sentences in the input file are separated either by a tab or by multiple
spaces.
4. File containing the sentences are looped until the end of the file and each
time similarity is computed for two sentences.
5. The sentences read are converted into English using a Goslate API.
6. During the process of finding similarity, sentences are initially converted
into individual tokens.
7. Then the tokens are stored in two vectors, which are passed as an input to
the get_Cosine function.
8. The function calculates the similarity score and returns the value.
9. Finally, the similarity scores of the sentences are stored in the output
file. 

Input Format : 

Sentence1 <tab> Sentence2 ( separated by tab or multiple spaces)

- first sentence (does not contain tabs)
- second sentence (does not contain tabs)

example : La cuerda es fuerte, es una cuerda gruesa <tab> Una sonrisa es una
          expresión que tú tienes en tu cara cuando estás agradecido,
          entretenido, o cuando estás siendo amistoso.
sentence 1 : La cuerda es fuerte, es una cuerda gruesa
sentence 2 : Una sonrisa es una expresión que tú tienes en tu cara cuando estás
             agradecido, entretenido, o cuando estás siendo amistoso.

-> sentences are first converted into English
Sentence 1 : The rope is strong, thick rope
Sentence 2 : A smile is an expression that you have on your face when you are
grateful , entertaining, or when you're being friendly .

Gold Standard:

The gold standard contains a score between 0 and 4 for each pair of sentences,
with the following interpretation:
(4) The two sentences are completely equivalent, as they mean the same thing.
(3) The two sentences are mostly equivalent, but some details differ.
(2) The two sentences are roughly equivalent, but some important  information
differs/missing.
(1) The two sentences are not equivalent, but are on the same topic.
(0) The two sentences are on different topics.

Output Format :
Similarity is returned as OutPut for the given sentences.
example : 3.52 (it can range from 0 - 4)
          3.25
          1.25
          0.27
          3.98  
'''

# os is a module which is used for passing command line arguments
# re is a module which helps in evaluating the regular expressions in the
# program. 
#It helps in implementing operations on regular expression
# math is a module which helps in evaluating the mathematical functions
import re, math, os, sys

#collections is a  container which implements specialized container data types
from collections import Counter
from Score import *

#Goslate is a python Google Translate API which has been used in the program to
# perform the translation of Spanish sentences 
#into English,which are then given a similarity score.
import goslate

# this is to create an instance of Goslate so as to perform the operation of
# translating the text from Spanish to English
gs=goslate.Goslate()
# Regex for detecting words in the sentence
WORD = re.compile(r'\w+')

def SubTask2_Baseline_Implement(source_file,output_file):
    ''' 
    This function takes in input of the sentence and returns the tokens for the
    sentence
    '''
    ofp = open(output_file, 'w')
    def text_to_vector(text):
        words = WORD.findall(text)
        return Counter(words)

    # Opens the file and gives an alias name fp to it
    with open(source_file) as fp:
        linenum = 1
        # to loop through each and every line in the file
        for line in fp:
            if line != '\n':
                sys.stdout.write("\rProcessing linenum: " + str(linenum))
                sys.stdout.flush()
                # to check if the line has two sentences separated by two or
                # more spaces instead of a tab, then we replace the spaces
                # with the tab in the line    
                if line.rfind('\t') == -1:
                    line = re.sub(r' {2,}', '\t', line)
                # splits the sentences divided by the tab and puts them in the
                # separate string left and right          
                left, right = line.split('\t', 1)
                # To translate the first sentence from Spanish to English. 
                # ( 'en' is to specify translation of a sentence into English)
                left = gs.translate(left, 'en')     
                # To translate the second sentence from Spanish to English. 
                # ( 'en' is to specify translation of a sentence into English)
                right = gs.translate(right, 'en')            
                # The sentence that is converted into English is stored in a 
                # vector as tokens 
                vector1 = text_to_vector(left)
                vector2 = text_to_vector(right)            
                # Finally both the vectors are passed as an arguments to the ge
                # Cosine which computes the similarity score for the 
                # pair of sentences
                cosine = get_cosine(vector1, vector2)
                # Cosine similarity is scaled to the scale 0 - 4
                cosine=cosine*4            
                # Similarity scores of all the sentences are written onto the
                # output file
                ofp.write(str(cosine) + "\n")
                linenum += 1
    # Close the file pointer
    ofp.close()