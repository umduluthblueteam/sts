'''
author :Charan Raj


This is the program which is implemented to read the data obtained from the 
GoslateTranslate.java and and parse the data from the text file it produced 
into a readabale format. initially the daata from the Important lists used in 
the program. The program takes the data in JSON format from which the possible 
translations of the given chunk are  retrieved as given out by the Google 
Translate API

chunklist -> is the list which contains list for two possible alternatives for the 
given spanish chunk of  the sentence
example of chunklist = [[u'According to', u'According'], [u'modern research', 
u'modern investigations'], [u','], [u'the sentence', u'the judgment'], 
[u'was not carried', u'not carried'], [u'out', u'place'], [u'and'], [u'Maria',
u'Mary'], [u'Anna Maria Schwegelin'], [u'was forgiven', u'was spared'], [u'.']]

englishlist -> is the list which contains two possible alternatives for the 
given spanish chunk, i.e in english example of english list = [u'The president
of the', u'The president of'] (with two most probable outcomes of a given chunk)

spanishlist -> spanish chunk for the corresponding english list
sentenceslengths -> list of sentenceslength 
sentence -> is a list which contains the possible sentneces formulated for 
given spanish sentnece


'''

import re
import json
# importing iter tools to permute thorugh the list to form all possible sentences
import itertools
import sys
# import pprint for debugging purposes
import pprint
import unicodedata
import os

# initializing the lists to empty
spanishlist =[]
englishlist =[]
sentenceslengths=[]

# counter for calculating the number of sentences
counter =0 
lengthlist=[]


# the file onto which the sentences are written
Final_output_file='/tmp/SpanishFinalnew'+sys.argv[1]+'.txt'
filewrite = open(Final_output_file,'w');
os.chmod(Final_output_file, 420)

# the input file from which the english chunks are extracted
Inputfile_to_parse="/tmp/Inputformanynew" + sys.argv[1]+ ".txt"

with open(Inputfile_to_parse) as file:
	# we want to parse every line in the file, so we split them by new line
	content= file.read().splitlines()

# To loop through every line in the input file	
for value in content:

	#initializing chunklist to empty which is the list which contains all the 
	# possible chunks for a given spanish chunk
	chunklist=[]
	counter = counter +1
	#print len(content)

	# convert the content in the line into a string
	value=str(value)
	
	# we perform a regulr expression on the given string to convert that
	# into the JSON format
	value = re.sub(',,+', ',"undefined",', value)
	
	# This is the try except block which takes care of the
	# strings which are not in JSON format
	try:
		jsonval =json.loads(value)
		#print "correct"
	except ValueError:
		pass

	# The possible translated words are located in the fifth position of the list,
	# so we copy that into a variable
	v = jsonval[5]
	#print json.dumps(v,indent =1)
	#print v[0][1]
	length = len(v)
	lengthlist.append(length)
	
	# This is the for loop to extarct the possible english chunks for the spanish
	# chunk.
	for i in v:
		
		# Spanish list conists of all the spanish words in the chunk
		spanishlist.append(i[0])
		listofwords=i[2]
		englishlist=[]

		# we are appending the most probable englisg translations of the spanish 
		# chunk. Here we are limiting the number of possible tranlations to two 
		# so that we limit the number of sentence ny the translator. We take 
		# the first two possible translations which are the most probable 
		# translations. Finally, we append the english lists to chunklist.

		for j in listofwords[:2]:
			#print j[0]
			englishlist.append(j[0])
		chunklist.append(englishlist)
	



	# This is the module which checks if the length of sentence has more 
	# than 25 chunks. If there are more than 25 chunks, then we limit number 
	# of words in the english list to one( now english list has only one word, 
	# which is the best possible translation).System runs very slow when we 
	# take  two words in english list, when there are more than 25 chunks.
	# we choose 25, as it gets harder to permute thought the list to form a 
	# sentence
	

	# to check if list has more than 25 chunks

	if len(chunklist) > 25:
		
		# loop throught every list in chunklist
		for possibilities in chunklist:
			if len(possibilities)==2:
				# remove the second word in every list of 
				# chunklist i.e, english list
				del possibilities[1]


	# Finding out the possible sentences for each spanish sentence, sentence is 
	# a list containing each sentence
	# sentence is formed by permuting through all the words in ech list
	sentence = [' '.join(s) for s in itertools.product(*chunklist)]	

		
	sentenceslength = len(sentence) 
	# To check the number of possible sentences formed 	
	sentenceslengths.append(sentenceslength)
	
	
	#print counter
	#  Here we are the limiting the maximum number of sentneces to 10. It 
	# becomes really hard to compute the similarity for over large number 
	# of sentences. We check if total sentnece formed are greater than 10. 
	# If it is greater, then we limit the number of sentences to ten

	if(sentenceslength>=10):
		sentenceslength=10
	else:
		sentenceslength=sentenceslength

	# We finally write the most probble translated sentences onto a seperate file, 
	# which is given as one of the input to the SubTasak.py 


	# loop until the length of all possible sentences of the given spanish sentence
	count=0
	while(count<sentenceslength):
		# write the sentence onto the file which is located in /tmp folder in the system
		writeStr = sentence[count]
		# Unicode string is encoded into ascii format for writing onto the file
		writeStr = unicodedata.normalize('NFKD', writeStr).encode('ascii', 'ignore')
		filewrite.write(writeStr) 
		
		# Every possible sentence is seperated by a tab space
		filewrite.write('\t')
		count= count+1


	# We then check if the sentence is the second sentence i.e, sentence of the pair of
	# spanish sentences

	if(counter%2==0):
		filewrite.write('\n')
	else:
		# sentences of the pair are seperated by three tab spaces
		filewrite.write('\t\t')

filewrite.close()
	