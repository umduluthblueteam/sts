'''
Created on Nov 21, 2014

@author: Saketh
File Description:
This file has the module that calculates the average of all labels
from the gold standard, that are used as the scores for paired
chunks in SUbTask3
Eg: If these are the averages,
    EQUI: 5
    SIMI: 3
    we assign this score to the chunks that are predicted with
    this label
'''

import re
def computeAverage(trainFile):
    '''
    This module computes the average score given a training file.
    For the 3rd SubTask, we compute the average of the scocres 
    for all the labels such as EQUI, OPPO, SIMI. etc...
    We then assign this score for the predicted labels.
    '''
    with open(trainFile) as fp:
        #Stores the scores and counts for each label in
        # dictionaries
        scoresDict = {'EQUI':0, 'SPE1':0, 'SPE2':0, 'OPPO':0, 'SIMI':0, 'REL':0}
        countDict = {'EQUI':0, 'SPE1':0, 'SPE2':0, 'OPPO':0, 'SIMI':0, 'REL':0}
        # iterate for every line in the gold standard
        for line in fp:
            #print line
            line = line.strip()
            # Regex for stripping the unwanted files
            pattern = re.compile("(.*//.*//.*//.*)")
            #Checks for the pattern in the read line
            if pattern.match(line):
                #The labels, scores are stored in the variables
                splits = line.split("//")
                keyLabel = splits[1].strip()
                valueScore = splits[2].strip()
                # As there are no scores for NOALI and ALIC,
                # we dont count them in our dictionary
                if (keyLabel != 'NOALI') & (keyLabel != 'ALIC'):
                    # If not, add them to the dictionary
                    scoresDict[keyLabel] = scoresDict[keyLabel] + float(valueScore)
                    countDict[keyLabel] = countDict[keyLabel] + 1
                    
        #print scoresDict
        #print countDict
        
        #Iterate the dictionary to compute the average for each label
        #and print it to console
        for key in scoresDict.keys():
            #Compute average only if the count id not zero
            if countDict[key] != 0:
                print key + " : " + str(scoresDict[key]/countDict[key])
            #Otehrwise it is not found in the train file
            else:
                print key + " : " + "Not found in the file"