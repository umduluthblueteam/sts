#python SubTask3.py ../Inputs/SubTask3/gold/STSint.gs.headlines.wa.source.input.test ../Inputs/SubTask3/gold/STSint.gs.headlines.wa.translate.input.test  headlines.raw.baseline baseline

#python SubTask3.py ../Inputs/SubTask3/gold/STSint.gs.images.wa.source.input.test ../Inputs/SubTask3/gold/STSint.gs.images.wa.translate.input.test images.raw.baseline  baseline

python SubTask3.py ../Inputs/SubTask3/gold/STSint.gs.headlines.wa.source.input.test ../Inputs/SubTask3/gold/STSint.gs.headlines.wa.translate.input.test headlines.raw.svm

python SubTask3.py ../Inputs/SubTask3/gold/STSint.gs.images.wa.source.input.test ../Inputs/SubTask3/gold/STSint.gs.images.wa.translate.input.test images.raw.svm

../Inputs/SubTask3/gold/wellformed.pl headlines.raw.baseline
../Inputs/SubTask3/gold/evalF1.pl ../Inputs/SubTask3/gold/STSint.gs.headlines.wa.test headlines.raw.baseline
../Inputs/SubTask3/gold/wellformed.pl images.raw.baseline
../Inputs/SubTask3/gold/evalF1.pl ../Inputs/SubTask3/gold/STSint.gs.images.wa.test images.raw.baseline
../Inputs/SubTask3/gold/wellformed.pl headlines.raw.svm
../Inputs/SubTask3/gold/evalF1.pl ../Inputs/SubTask3/gold/STSint.gs.headlines.wa.test headlines.raw.svm 
../Inputs/SubTask3/gold/wellformed.pl images.raw.svm
../Inputs/SubTask3/gold/evalF1.pl ../Inputs/SubTask3/gold/STSint.gs.images.wa.test images.raw.svm



ON TEST DATA:
 time python SubTask3.py ../Inputs/SubTask3/raw/STSint.input.images.sent1.txt ../Inputs/SubTask3/raw/STSint.input.images.sent2.txt ../Outputs/SubTask3/images_nochunked_nobaseline.wa nochunked nobaseline

 time python SubTask3.py ../Inputs/SubTask3/raw/STSint.input.headlines.sent1.txt ../Inputs/SubTask3/raw/STSint.input.headlines.sent2.txt ../Outputs/SubTask3/headlines_nochunked_nobaseline.wa nochunked nobaseline

 time python SubTask3.py ../Inputs/SubTask3/chunked/STSint.input.images.sent1.chunk.txt ../Inputs/SubTask3/chunked/STSint.input.images.sent2.chunk.txt ../Outputs/SubTask3/images_chunked_nobaseline.wa chunked nobaseline

 time python SubTask3.py ../Inputs/SubTask3/chunked/STSint.input.headlines.sent1.chunk.txt ../Inputs/SubTask3/chunked/STSint.input.headlines.sent2.chunk.txt ../Outputs/SubTask3/headlines_chunked_nobaseline.wa chunked nobaseline