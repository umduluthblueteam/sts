'''
Created on Nov 11, 2014

@author: viswanadh
'''
import re
import pprint
import logging

##A logging class for pretty printing log output

class pretty_logger:
    def __init__(self, name):
        self.log = logging.getLogger(name)
        self.pp = pprint.PrettyPrinter()

    def process(self, *str):
        tmp = ""
        for arg in str:
            tmp+=(self.pp.pformat(arg)+" ")
        return tmp

    def pretty_debug(self, *str):
        self.log.debug(self.process(*str))

    def pretty_critical(self, *str):
        self.log.critical(self.process(*str))

    def pretty_info(self, *str):
        self.log.info(self.process(*str))
