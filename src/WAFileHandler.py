"""  
Author: Viswanadh
Module for serializing and deserializing between WA(V.6.B in README.txt ) 
and WADS (I.2. in src/SubTask3.py)
"""
###Modules from python standard library
# re for regular expressions
import re
# sys for command line arguments 
import sys

###Modules written by us
# Importing for pretty print
from utils import *

def write_wa_file(aligned_chunks_with_types_and_scores, output_file):
  '''
    Method to serialize a WA data structure to a WA file
    :param - aligned_chunks_with_types_and_scores - WADS
    :param - output_file - output file path 
  '''
  out = open(output_file, 'w')

  for sentence_pair in aligned_chunks_with_types_and_scores:
    #Lines to be written for each sentence pair are stored in a tuple joined 
    #and joined by a newline at the end and written to the output wa file
    xmlString = list()
    
    # sentence tag
    xmlString.append('<sentence id="{id}" status="{status}">'.
      format(id=sentence_pair['id'], status = sentence_pair['status']))
    
    #source sentence
    xmlString.append('// '+sentence_pair['source_sentence'])
    #translation sentence
    xmlString.append('// '+sentence_pair['translation_sentence'])
    
    #source tag containing a numbered list of words in source sentence
    xmlString.append('<source>')
    for index, source_word in enumerate(sentence_pair['source_wordlist']):
      xmlString.append(str(index+1)+" "+source_word+" :")
    xmlString.append('</source>')
    
    #translation tag containing a numbered list of words in translation sentence
    xmlString.append('<translation>')
    for index, translation_word in enumerate(
                  sentence_pair['translation_worlist']):
      xmlString.append(str(index+1)+" "+translation_word+" :")
    xmlString.append('</translation>')

    #alignment tag containing information about chunk alignments
    xmlString.append('<alignment>')
    for alignment in sentence_pair['alignments']:
      xmlString.append('%s <==> %s // %s // %s // %s <==> %s' % (
          alignment['source_chunk_word_indices'],
          alignment['translation_chunk_word_indices'],
          alignment['type'],
          alignment['score'],
          alignment['source_chunk'],
          alignment['translation_chunk'],

      ))
    xmlString.append('</alignment>')
    xmlString.append('</sentence>\n\n')
    #join the output lines in the tutple and write it to the output file
    out.write('\n'.join(xmlString))
  out.close();

def read_wa_file(xmlfile):
  """
    Method to deserialize a WA file into WADS
    :param - xmlfile - WA file 
    :return - WADS 
  """
  #regex to check if the line read is a sentence
  sentence_regex = '<sentence id="(\d+)" status="(\w*)"'
  # Initilize a custom logger written by us for logging
  pl = pretty_logger(__name__)
  pretty_debug = pl.pretty_debug 
  pretty_debug(sentence_regex)
  
  #WADS 
  XMLs = list()
  with open(xmlfile) as wa_file:
    done = False
    def readline():
      '''
          A nested function to read non empty lines from WA file
      '''
      while True:
        line = wa_file.readline()
        if line!="":
          line = line.strip();
          if len(line)!=0:
            pretty_debug('line: '+line)
            return line
        else:
          done = True 
          wa_file.close()
          return
    
    while not done:
      line = readline()
      if not line:
        break
      #Sentence tag found
      if line.find('<sentence') != -1:
        XML = dict()
        pretty_debug("new XML class started")
        match = re.match(sentence_regex, line) 
        id = match.group(1)
        pretty_debug('id = '+ id)
        XML['id'] = id
        status = match.group(2)
        pretty_debug('status = '+ status)
        XML['status'] = status
        #read left and right sentences
        left = readline().split('//')[1].strip()
        pretty_debug("left = "+ left)
        XML['source_sentence'] = left
        right = readline().split('//')[1].strip()
        pretty_debug("right = "+ right)
        XML['translation_sentence'] = right
      #source tag found  
      elif line.find('<source>')!=-1:
        pretty_debug("Reading source word list")
        source_word_list=list()
        #Read source wordlist
        while True:
          line=readline();
          #When we have reached end of source word list
          if line.find('</source>')!=-1:
            break
          else:
            #strip the number part for each word
            source_word_list.append(line.split(" ")[1])
        pretty_debug("source word list = " ,source_word_list)
        XML['source_wordlist'] = source_word_list;
      #translation tag found
      elif line.find('<translation>')!=-1:
        pretty_debug("Reading translation word list")
        translation_word_list= list()
        #Read translation wordlist
        while True:
          line=readline();
          #When we have reached end of source word list
          if line.find('</translation>')!=-1:
            break
          else:
            #strip the number part for each word
            translation_word_list.append(line.split(" ")[1])
        pretty_debug("translation world list = ", translation_word_list)
        XML['translation_worlist'] = translation_word_list
      #alignment tag found
      elif line.find('<alignment>')!=-1:
        pretty_debug("Reading alignments list")
        alignments = list()
        #read the alignments information
        while True:
          line = readline()
          #When we have reached end of alignments list
          if line.find('</alignment>')!=-1:
            break
          else:
            #fetch information about the chunk alignment
            splits = line.split('//')
            alignment = dict()
            word_indices = splits[0].split('<==>')
            alignment['source_chunk_word_indices'] = word_indices[0].strip()
            alignment['translation_chunk_word_indices'] =word_indices[1].strip()
            alignment['type'] = splits[1].strip().split("_")[0]
            alignment['score'] = splits[2].strip()
            chunks = splits[3].split('<==>')
            alignment['source_chunk'] = chunks[0].strip();
            alignment['translation_chunk'] = chunks[1].strip();
            alignments.append(alignment)  
        pretty_debug("alignments = ", alignments)
        XML['alignments'] = alignments

      elif line.find('</sentence>')!=-1:
        #add the dictionary populated for each sentence to the final return list
        XMLs.append(XML)
  return XMLs

#for testing the module, Please ignore this
if __name__=='__main__':
  if len(sys.argv)<2:
    print "Needs a wa file as input"
    sys.exit(-1)
  XMLs = read_wa_file(sys.argv[1])
  pdebug = get_pretty_debug()
  pdebug(XMLs)
