#-*- coding: utf-8 -*- 
'''
Author: Charan Raj

Task Description:

Given two sentences of text, s1 and s2, the systems participating in this task 
should compute how similar s1 and s2 are, returning a similarity score, and an
optional confidence score. The scores  should range from 0 to 4, where 4 marks 
paraphrases, and 0, sentences that have absolutely no relation.

Program Description

The program written below shows how the similarity is calculated given a set of 
Spanish sentences. 
1. Initially name of the file is given as an command line arguments.
2. File consisting of number of Spanish sentences are read, Each line in the 
file has two sentences for which similarity is to be evaluated.
3. The sentences in the input file are separated either by a tab or by multiple 
spaces. 
4. File containing the sentences are looped until the end of the file and each 
time similarity is computed for two sentences.
5. The sentences read are converted into English using a Bingtranslator API 
and google Translate API.
6. This process iss plit into two modules
    1. Translating spanish sentences using Bingtranslator API which returns a 
    single sentnece (most probable sentence)
    2. Translating a sentence using Google Translate API, along with the best 
    possible translation, it gives out the possible translations of every chunk 
    in the spanish sentence, from which we not only get the best possible 
    translation, we get the other probable translations for the spanish sentence.
    The best possible sentences generation is three step process, where the 
    input file first goes to GoslateTranslate.java program which generates an 
    intermediate file, which is passed as an input to the InputParsing.py which 
    retrieves the sentencesfrom the intermediate file. GoslateTranslate.java and
    InputParsing.py have further details on how the sentences are retrieved. 
7. Two sentences are then passed as an arguments to the aligner tool which
aligns similar words from the two sentences and returns an alignment list 
which contains a list of similar words.
8. So as to remove the stop words, which affect the efficiency of the similarity 
scores, we remove them by looping through the alignment.
Stop words are removed along with the punctuation.
9. Similarity score is calculated for the sentences according to the formula 
mentioned in the "Sentence Similarity from Word Alignment" Paper,The scoring 
function in score.py returns the similarity score.
10. Finally, the similarity scores of the sentences are stored in the output file.

Input Format : Sentence1 <tab> Sentence2 (separated by tab or multiple spaces)

- first sentence (does not contain tabs)
- second sentence (does not contain tabs)


example : (La cuerda es fuerte, es una cuerda gruesa <tab> Una sonrisa es una 
    expresión que tú tienes en tu cara 
cuando estás agradecido, entretenido, o cuando estás siendo amistoso
sentence 1 : La cuerda es fuerte, es una cuerda gruesa
sentence 2 : Una sonrisa es una expresión que tú tienes en tu cara cuando estás 
agradecido, entretenido, o cuando estás siendo amistoso.


-> sentences are first converted into English

Sentence 1 : The rope is strong, thick rope
Sentence 2 : A smile is an expression that you have on your face when you are 
grateful , entertaining, or when you're being friendly .

Gold Standard:
The gold standard contains a score between 0 and 4 for each pair of sentences, 
with the following interpretation:
(4) The two sentences are completely equivalent, as they mean the same thing.
(3) The two sentences are mostly equivalent, but some details differ.
(2) The two sentences are roughly equivalent, but some important  information 
differs/missing.
(1) The two sentences are not equivalent, but are on the same topic.
(0) The two sentences are on different topics.

Output Format :

Similarity is returned as OutPut for the given sentences.
example : 3.52 (it can range from 0 - 4)
          1.24
          0.24    
          3.56
          
Scoring:
The official score is based on the average of Pearson correlation.
Please refer to SubTask 1 for more details on Pearson correlation
 '''

# os is a module which is used for passing command line arguments
# re is a module which helps in evaluating the regular expressions in the program. 
#It helps in implementing operations on regular expression
import os,re, sys

import subprocess as sub
# To append current directory to the python Path
os.sys.path.insert(1, '../monolingual-word-aligner')
from aligner import *
import unicodedata
from microsofttranslator import Translator
# Unidecode is used to perform the operation of converting Unicode that into 
# ASCII characters
from unidecode import unidecode

# We are using the Score function mentioned in the Score file, which computes 
# the similariry and returns the sscore
# in the range of 0-1
from Score import *

# Here we are creating an instance of Bing Translator API, where we requested the
# Client ID and Client secret from the web page
# "https://datamarket.azure.com/dataset/bing/microsofttranslator ". These are 
# passed as arguments to create an instance of Translator.
# The number of characters that can be used for every month are 20,000,00 
# characters. We can check the amount of data used in the same web page.
# We initially have to register our system (Semantic textual Similarity ) in the
# same web page so as to obtain the client secret. We need to assign a name to 
# Client ID

translator = Translator('Blue1234', '1Sj91CWgtigim8jFKoTqTBYiUtU+zazWpEDmNWZS8CU=')

def SubTask2_implement(source_file,output_file,count):

    
    sim2=[]
    # list for storing the similarity score using the Bing translator

    bingsim=[]
    # list for storing the similarity score using the GoogleTranslate API
    Googlesim=[]

    count=str(count)
    Google_Input_file='/tmp/SpanishFinalnew'+count+".txt"
    ofp = open(output_file, 'w')

    '''
    Module1

    Works with Bingtranslator API
    -------
    '''
    with open(source_file) as fp: 
        counterbing = 1
            # to loop through each and every line in the input file
        for line in fp:
           # print line
            if line != '\n':
                    # to check if the line has two sentences separated by two or
                    # more space instead of a tab, then we replace the spaces
                    # with the tab in the line
            
                if line.rfind('\t') == -1:
                    line = re.sub(r' {2,}', '\t', line)
        
                # splits the sentences divided by the tab and puts them in the 
                # separate string left and right
               
                
                left, right = line.split('\t', 1)
                
                # To translate the first sentence from Spanish to English. 
                # ( 'en' is to specify translation of a sentence into English)    
                sys.stdout.write("\rProcessing linenum: " + str(counterbing) + " using Bing")
                sys.stdout.flush()# The input to the bing translator should be in the unicode
                # format. Hence decode it from utf-8 to unicode
                left = translator.translate(left.decode("utf-8"),'en','es')
                # The unicode is now encoded to ascii format, as the aligner
                # tool works only with the ascii characters
                left = unicodedata.normalize('NFKD', left).encode('ascii', 'ignore')
                
                # To translate the second sentence from Spanish to English. 
                # ( 'en' is to specify translation of a sentence into English)    
                # The input to the bing translator should be in the unicode
                # format. Hence decode it from utf-8 to unicode
                right = translator.translate(right.decode("utf-8"),'en','es')
                # The unicode is now encoded to ascii format, as the aligner
                # tool works only with the ascii characters
                right = unicodedata.normalize('NFKD', right).encode('ascii', 'ignore')
                
                # after converting the sentences into English, sentences are 
                # passed as an argument to the aligner which returns list of
                # similar words in two sentences  
                
                alignments = align(left,right)
                sim = scoring(alignments)
                bingsim.append(sim)
                counterbing += 1


    '''
    Module 2
    --------
     This module  takes the input from the file which is generated by InputParsing.py. 
     The file which was generated by Input Parsing.py consists of multiple 
     sentences for each spanish sentence which are seperated by tab spaces and 
     then they are seperated by the multiple tab spaces of the second sentence 
     of the pair.

     Here is the example for the format of the file from which the sentences are 
     taken.

     Example :


     A rooster is an adult male chicken .   A rooster is an adult male chick .  A rooster are an adult male chicken .   
     A rooster are an adult male chick . A cock is an adult male chicken .   A cock is an adult male chick . 
     A cock are an adult male chicken .  A cock are an adult male chick ."           "A journey is a long journey by boat 
     or a spaceship. A journey is a long journey boat or a spaceship.    A journey is a long trip by boat or a spaceship.    A 
     urney is a long trip boat or a spaceship.   A cruise is a long journey by boat or a spaceship.  A 
     cruise is a long journey boat or a spaceship.  A cruise is a long trip by boat or a spaceship. A cruise is a long 
     trip boat or a spaceship."

     The above information is a line in the input file genereated by 
     InputParsing.py, where multiple translations of the same sentence are 
     separated by a tab space and there is a three tab space between the 
     sentences of the pair.

     Each pair of sentences are separated by a new line.

     Initially in the module each sentence is read and similarity is computed 
     for the corresponding sentences of the pair. We are limiting the number of 
     translations to 10 if there are more than 10 translations for both the pair 
     of the sentences. This is done by comparing the number of sentences written 
     in the Inputfile genereated by InputParsing.py

    '''


# This is the file which contains the sentnces parsed from InputParsing.py file

    # counter to check the number of sentences
    
    with open(Google_Input_file) as final:

        #to loop through all the sentences in the multiple translations file
        # every pair of sentnces are stored in a separate line
        countergoogle = 0
        for lines in final:
            sys.stdout.write("\rProcessing linenum: " + str(countergoogle+1) + " using Google")
            sys.stdout.flush()# The input to the bing translator should be in the unicode
                
            # Two lists are initialized to empty to store the sentnces of a pair 
            # of spanish sentneces
            #Initially the sentences are stored in the two list i.e,  sentnces 
            # multiple translations are store in left 
            # list and right list respectivley
            left2=[]

            right2=[]
            
            # counter to check for the number of sentnces in the file and 
            # incrementing it for every sentence
            countergoogle= countergoogle+1
            # left sentence English translations are stored in left list and 
            # right sentence multiple translations 
            # are stored in right list
            # left and right pairs are split using 3 tab spaces as it was 
            # seperated by three tab spaces
            left2,right2=lines.split('\t\t\t')
            
            #After sentneces are store in left and right list, now every 
            # sentence of left2 list and right2 list are stored in 
            # another lists named leftsent and rightsent respectively, 
            # since sentnces in those list are seperated by tab spaces
            # as described in the above example
            leftsent= left2.split('\t')
            rightsent=right2.split('\t')
            
            rightsent.remove('\n')

            # Below are the conditional statements which is used to reduce the 
            # number of sentnces obtained from multiple translations. We are 
            # limiting the number of comparisons to ten. Suppose there are 4 
            # sentences for the the first spanish sentence and there are ten  
            # sentences for the other spanish sentence in the pair, then we 
            # limit our comparison to four, as we are comparing the corresponding 
            # sentences of the both the sentence pairs. 

            leftlength=len(leftsent)
            rightlength=len(rightsent)

            # check to see which of the translations have more sentences
            # length is used to store the minimum number of sentences in the pair
            if(leftlength>rightlength):
                length=rightlength
            else:
                length=leftlength

                # we initially assign the list named alignments2 to empty, value 
                # returned from the  align function is stored in the list
                # sim2 is the list which contains the similarity scores the 
                # corresponding sentences compared from the leftsent and rightsent

            alignments2=[]
            sim2=[]

            # this is the for loop  which is used to compare the similarity 
            # scored for each corresponding sentence in both the lists 
            # (leftsent and rightsent)
            for i in range(length):
                # left3 is every sentence in the leftsent list and right3 is 
                # every sentence in the rightsent list
                left3=leftsent[i]
                right3=rightsent[i]
                # sentences are passed as an argument to the aligner which 
                # returns list of similar words in two sentences 
                alignments2=align(left3,right3)
                # After that the alignmnetss is passed to the scoring function 
                # which is present in Score.py, which calculates the similarity 
                # score of two sentences
                score=scoring(alignments2)
                # similarity scores for all the sentnces in leftsent and right 
                # sent are stored in a list called sim2
                sim2.append(score)
            
            # this while loop is used to calculate the average of the similarity 
            # scores obtained for all the sentences in the leftsent list and 
            # rightsent list

            sum=0  
            counter2=0
            while(length>0):
                sum= sum + sim2[length-1]
                counter2=counter2+1
                length=length-1
            
            sum = sum/counter2
            # Finally, similarity score for all the sentences for all the 
            # sentences are stored in the list called Googlesim

            Googlesim.append(sum)
            #print len(Googlesim)
        
    # This for loop calculates the average of similarity score for the pair of 
    # the spanish sentences using the 
    # Microsoft Bing Translator and the Google Translate API and writing the 
    # similarity scores onto a file
    for j in range(countergoogle):
        finalsim=(bingsim[j]+Googlesim[j])/2
        finalsim= finalsim*4
        ofp.write(str(finalsim)+'\n')

    ofp.close()
