'''
Filename: SubTask1.py
Author: Saketh

Task Description:
Given a pair of English sentences, our system returns a Similarity score on a
scale from 0 to 5, where 0 indicates there is absolutely no relation between the
sentences and 5 indicates paraphrases.

Data for SubTask 1:
ENGLISH STS: It contains sentence pairs in English extracted from:
1) image description (image)
2) news headlines (headlines)
3) student answers paired with reference answers (answers-students)
4) answers to questions posted in stack exchange forums (answers-forum)
5) English discussion forum data exhibiting committed belief (belief)

Input format:
The input file consist of two fields separated by tabs:
- first sentence (does not contain tabs)
- second sentence (does not contain tabs)
Format: Sentence1 <tab> Sentence2 (separated by tab or multiple spaces)

Gold Standard:
The gold standard contains a score between 0 and 5 for each pair of sentences,
with the following interpretation:
(5) The sentences are equivalent
(4) The sentences are almost eqiuivalent, but differ in few details
(3) The sentences are little equivalent, many details might differ
(2) The sentences are not equivalent, but has some common information
(1) The sentences are not equivalent, but they are about same topic
(0) The sentences are completely irrelevant
      
Answer format:
The answer format is similar to the gold standard format.
- a number between 0 and 5 (the similarity score)

Output Format :
Similarity is returned as OutPut for the given sentences.
example : 4.52 (it can range from 0 - 5)
          1.24
          0.24    
          3.56

Scoring:
The official score is based on the average of Pearson correlation.
The Pearson correlation takes the gold standard as X coordinate, our
output as Y coordinate and plots a graph. The Pearson correlation denotes
how well the data points can be fitted on the straight line X=Y. If our output
matched the gold standard completely, the PC will be 1.

OUR APPROACH:
============
We compute the similarity of two sentences using a tool called monolingual-
word-aligner. Given a pair of sentences, the tool takes a lot parameters into
account for aligning the words from each sentence.

It follows a sequence of steps to align the words. They are:
1. Firstly, it aligns identical word sequences
2. Then, it aligns named entities
3. And then, content words
4. Lastly, it aligns the stop words

In the process of aligning the words, it takes contextual evidence into
consideration using the syntactic dependencies (using the parse trees) and
textual neighbourhood.

Once the word alignments are obtained, we compute the semantic similarity
using the harmonic mean of the aligned content proportion of two sentences.
This proportion is the ration of aligned content words to the total content
words in the sentence.

This harmonic mean is then scaled to the range 0 - 5, to meet the organizers
requirement.
'''

# os is a module which is used for passing command line arguments
# re is a module which helps in evaluating the regular expressions in the
#program. It helps in implementing operations on regular expression
import os, sys
import re
import unicodedata

# To append current directory to the python Path
os.sys.path.insert(1, '../monolingual-word-aligner')
# This is the aligner tool which we have used to perform the process of aligning
# similar words in the sentences.
from aligner import *
from Score import *

def SubTask1_implement(source_file,output_file):
    '''
    This function takes in the source_file and output_file as input.
    It reads the sentences line by line, computes the similarity between the two
    sentences (seperated by tabs in each line) and writes it to the output_file.
    '''
    def preProcess(line):
        '''
        This module does the necessary preprocessing before aligning the words.
        The input might contain few Latin characters and they have to be
        encoded into ASCII format.
        '''
        line = line.decode('iso-8859-2')
        line = unicodedata.normalize('NFKD', line).encode('ascii', 'ignore')
        return line
    # Opens the output file and writes the similarity scores into the output
    # file
    ofp = open(output_file, 'w')
    # Opens the file and gives an alias name fp to it
    with open(source_file) as fp:
        linenum = 1
        for line in fp:
            if line != '\n':
                sys.stdout.write("\rProcessing linenum: " + str(linenum))
                sys.stdout.flush()
    
                # to check if the line has two sentences separated by two or
                # more space instead of a tab, then we replace the spaces
                if line.rfind('\t') == -1:            
                    # Replace consecutive spaces (with more than 2 spaces)
                    # with the tab in the line
                    line = re.sub(r' {2,}', '\t', line)

                # Splits the sentences divided by the tab and puts them in
                # separate strings left and right            
                sents = line.split('\t')
                right = sents[-1]
                left = sents[-2]

                left = preProcess(left)
                right = preProcess(right)
                # Sentences are passed as an argument to the aligner which
                # returns list of similar words in two sentences    
                
                # Stanford corenlp closes connections sometimes, so keep trying
                # for 10 times. 
                done = False
                tryCounter = 0
                while not done and tryCounter < 10:
                    try:
                        alignments = align(left, right)
                        # The similarity is computed using the scoring function
                        sim = scoring(alignments)
                        done = True
                    except:
                        sim = 0
                        tryCounter += 1
                        pass

                
                # Scale it to the scale of 0 - 5
                sim= sim *5

                # To write the similarity score of both sentences into a file           
                ofp.write(str(sim) + "\n")
                linenum += 1

    # Close the file pointer
    ofp.close()
