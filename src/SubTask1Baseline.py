'''
Filename: SubTask1Baseline.py
Author: Saketh

Task Description:       |
Data for SubTask 1:     |
Input format:           |---|\  Please refer to
Gold Standard:          |---|/  SubTask 1
Answer format:          |
Scoring:                |

BASELINE APPROACH:
==================
    In order to provide a simple word overlap baseline, first tokenize the input
sentences splitting on white spaces, and then represent each sentence as a
vector in the multidimensional token space. Each dimension has 1 if the token
is present in the sentence, 0 otherwise. Vector similarity is computed using the
cosine similarity metric.
'''

# os is a module which is used for passing command line arguments
# re is a module which helps in evaluating the regular expressions in the
# program. 
# It helps in implementing operations on regular expression
# math is a module which helps in evaluating the mathematical functions
import re, math, os, sys
# collections is a  container which implements specialized container data types
from collections import Counter
# for computing cosine similarity
from Score import *

# Regex for detecting words in the sentence
WORD = re.compile(r'\w+')

def SubTask1_Baseline_Implement(source_file,output_file):
    '''
    This function takes in the source_file and output_file as input.
    It reads the sentences line by line, computes the similarity between the two
    sentences (seperated by tabs in each line) and writes it to the output_file.
    '''
    ofp = open(output_file, 'w')
    def text_to_vector(text):
        words = WORD.findall(text)
        return Counter(words)

    # Opens the file and gives an alias name fp to it
    with open(source_file) as fp:
        # to loop through each and every line in the file
        linenum = 1
        for line in fp:
            if line != '\n':
                sys.stdout.write("\rProcessing linenum: " + str(linenum))
                sys.stdout.flush()

                # to check if the line has two sentences separated by two or
                # more spaces instead of a tab, then we replace the spaces
                # with the tab in the line            
                if line.rfind('\t') == -1:
                    line = re.sub(r' {2,}', '\t', line)
                # splits the sentences divided by the tab and puts them in the
                # separate string left and right  
                sents = line.split('\t')
                right = sents[-1]
                left = sents[-2]
                #left, right = left.lower(), right.lower()
                left_words = ''
                right_words = ''
                # for word in left.split(" "):
                #     if word not in stops:
                #         left_words+=word+ ' '


                # for word in right.split(" "):
                #     if word not in stops:
                #         right_words+=word + ' '

                # The sentence that is converted into English is stored in a
                # vector as tokens 
                vector1 = text_to_vector(left)
                vector2 = text_to_vector(right)
                # Finally both the vectors are passed as an arguments to the
                # get_cosine which computes the similarity score for the 
                # pair of sentences
                cosine = get_cosine(vector1, vector2)
                cosine=cosine*5
                # Similarity scores of all the sentences are written onto the
                # output file
                ofp.write(str(cosine) + "\n")
                linenum += 1
    # Close the file pointer
    ofp.close()