//@author: saketh
/*
This java class reads in the raw input file (the actual input) in ascii/iso-8859-2
format and uses Opennlp chunker to chunk the sentences. In the process, it does
tokenization and pos tagging. All the required information such as the chunk tags,
words parts of speech are written to a file, that are later read by a python module.

The Opennlp chunker seem to do a better job than MBSP chunker and NLTK chunker.
The chunking accuracy is compared with the gold chunks provided with the training
data.

Input format:
<One sentence per line>
Eg:
.
.
.
A man sitting in a cluttered room .
Black cow walking under trees in pasture .
A smiling woman with a beer sitting outside with another smiling woman .
a woman with a big necklace .
Brown and white cow standing in grass at side of road .
A jockey riding a horse in a pen .
A person wearing a helmet rides a bike near a white structure ..
.
.

Output format:
<One sentence per line>
Eg:
.
.
.
|||B-NP****A@@DT****brown@@JJ****horse@@NN|||B-PP****in@@IN|||B-NP****a@@DT
****green@@JJ****field@@NN|||O****.@@.

|||B-NP****The@@DT****old@@JJ****lady@@NN|||B-VP****is@@VBZ****standing@@VBG
|||B-PP****in@@IN|||B-NP****the@@DT****kitchen@@NN|||B-PP****with@@IN|||B-NP
****two@@CD****cats@@NNS|||B-PP****at@@IN|||B-NP****her@@PRP$****feet@@NNS
|||O****.@@.

|||B-NP****Double@@JJ****decker@@NN****passenger@@NN****bus@@NN|||B-VP****
driving@@VBG|||B-PP****with@@IN|||B-NP****traffic@@NN|||O****.@@.

|||B-NP****A@@DT****man@@NN|||B-VP****lies@@VBZ|||B-PP****in@@IN|||B-NP****
a@@DT****bed@@NN|||B-PP****in@@IN|||B-NP****a@@DT****cluttered@@JJ****room
@@NN|||O****.@@.

|||B-NP****Cow@@NNP|||B-VP****walking@@VBG|||B-PP****under@@IN|||B-NP****the
@@DT****tree@@NN|||B-PP****in@@IN|||B-NP****a@@DT****pasture@@NN|||O****.@@.

|||B-NP****Two@@CD****women@@NNS|||B-VP****sitting@@VBG|||B-NP****outside@@JJ
****laughing@@NN|||O****.@@.
.
.
.

*/

//Import packages required for implementation
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.cmdline.PerformanceMonitor;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.Span;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;

//This class has all the required functionality
public class OpennlpTools {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InvalidFormatException 
	 */

	//Main method that calls the chunk method using the command line arguments
	public static void main(String[] args) throws InvalidFormatException, IOException {
		// TODO Auto-generated method stub
		//Make sure the input and output file is passed while running it
		if(args.length==2){
			//0 has the input file name and 1 has the output file name
			chunk(args[0], args[1]);
		}else{
			//Throw an error if there are not enough arguments
			System.err.println("Need an input and output file");
		}
	}
	
	//This methos takes the input file and writes the chunked output to
	//output file name. It uses few Opennlp models that are pre trained to do 
	//pos tagging and chunking
	public static void chunk(String fileName, String output_fileName) throws IOException {
		//Load the POS tagger module
		POSModel model = new POSModelLoader()
				.load(new File("../Opennlp/Models/en-pos-maxent.bin"));
		//Initialize the tagger
		POSTaggerME tagger = new POSTaggerME(model);
		//Load the chunker module as input stream
		InputStream is = new FileInputStream("../Opennlp/Models/en-chunker.bin");
		//Initialize the chunker model
		ChunkerModel cModel = new ChunkerModel(is);
		ChunkerME chunkerME = new ChunkerME(cModel);
		//Read the file in ISO-8859-2 format, otherwise the encoding information is lost
		//This is because the input file may have Latin characters and reading the file
		//in ascii format may loose those characters
		BufferedReader br = new BufferedReader(
								new InputStreamReader(
									new FileInputStream(fileName), "ISO-8859-2"));
		//Write the file in utf-8 format
		PrintWriter writer = new PrintWriter(output_fileName, "UTF-8");
		String line;
		//Read the line by line from the file
		while ((line = br.readLine()) != null) {
			//process the line.
			//line is in ISO-8859-2 encoding
			//input is in Unicode format. String by default is in Unicode format
			String input = line;
			//normalize the unicode format using NFD 
			String line_norm = Normalizer.normalize(input, Normalizer.Form.NFD);
			//this pattern will recognize diacritical marks
			String regex = "[\\p{InCombiningDiacriticalMarks}\\p{IsLm}\\p{IsSk}]+";
			//Delete the diacritical marks and get the byte stream in ASCII encoding,
			//then convert it into string
		    String goldStr = new String(line_norm.replaceAll(regex, "").getBytes("ascii"), "ascii");
			//Create an object stream using the string obtained
			ObjectStream<String> lineStream = new PlainTextByLineStream(
												new StringReader(goldStr));
	
			String lineToken;
			//This array will have the tokenized sentence
			String whitespaceTokenizerLine[] = null;
		 
		 	//This array will hold the pos tags for all the tokens obtained
			String[] tags = null;
			//Tokenize the line and then tag every token
			while ((lineToken = lineStream.read()) != null) {
				whitespaceTokenizerLine = WhitespaceTokenizer.INSTANCE
						.tokenize(lineToken);
				tags = tagger.tag(whitespaceTokenizerLine);
		 
			}
			//Length of tokens array
			int tokensLength = whitespaceTokenizerLine.length;
			//It holds the chunked output of chunker
			String result[] = chunkerME.chunk(whitespaceTokenizerLine, tags);
			//Once the chunks are avilable, write the chunks to the output file
			//in any custom format.
			//This format is used for splitting the chunks in Python module
			for (int i=0; i< tokensLength; i++) {
				//A chunk always starts with B - Beginning or O - Outside
				if(result[i].startsWith("B")||result[i].startsWith("O"))
				{	
					//Write it to the file
					writer.print("|||" + result[i]);
				}
				//Seperate the tags from words using a delimiter
				writer.print("****" + whitespaceTokenizerLine[i] + "@@" + tags[i]);
			}
			//Insert a new line for every line
			writer.println();
		}
		//Close the bufferred reader and file writers
		br.close();
		writer.close();
	
	}
	
}
