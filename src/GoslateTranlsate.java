// author : Charan Raj
/*

This is the Java program which is resposnsible for finding the  multiple translations
of the given spanish sentence. This program genenrates an output file onto which the 
sentences are written as a JSON object from which the translationsare retireved using 
the InputParsing.py program. 
Initially sentnece are read from the Input file, then we try to find the multiple tranlstions
of a chunk using translate.translateCompleteResult() method of Google API.
After the translations the output is written onto a fil, where each line has translations
of a single spanish sentence.
The output generetad by this program is written ina new line every line, where each line
has multiple translations of a sentence
Input to the file is taken directly from the Input folder, where the input is file of
pair of spanish sentences. 

Input format :
--------------
El "Parlamento de Fiyi" ("Parliament of Fiji" en inglés), es bicameral y lo forman la
Cámara de Representantes y el Senado de Fiyi.		El Parlamento de Fiyi nace el 10 
de octubre de 1970, cuando Fiyi consiguió la independencia del Reino Unido.

Two sentences are seperated by a tab space

Output generated by the file:
-----------------------------
[[["\"My Scene\" is a line of dolls from Mattel Barbie derived from commercially 
launched to compete with Bratz dolls company MGA .","\"My Scene\" es una línea 
de muñecas de la empresa Mattel derivada de Barbie lanzada para competir comercialmente 
con las muñecas Bratz de la empresa MGA.","",""]],,"es",,[["\"My",[1],true,false,428,0,2,0],
["Scene\"",[2],true,false,753,2,4,0],["is a line of",[3],true,false,671,4,8,0],
["dolls from",[4],true,false,537,8,10,0],["Mattel",[5],true,false,588,10,11,0],
["Barbie",[6],true,false,473,11,12,0],["derived from",[7],true,false,231,12,14,0],
["commercially",[8],true,false,400,14,15,0],["launched",[9],true,false,303,15,16,0],
["to compete",[10],true,false,372,16,18,0],["with",[11],true,false,362,18,19,0],
["Bratz",[12],true,false,360,19,20,0],["dolls",[13],true,false,155,20,21,0],
["company",[14],true,false,213,21,22,0],["MGA",[15],true,false,545,22,23,0],
[".",[16],false,false,546,23,24,0]],[["\" My",1,[["\"My",428,true,false]],[[0,3]],
"\"My Scene\" es una línea de muñecas de la empresa Mattel derivada de Barbie 
lanzada para competir comercialmente con las muñecas Bratz de la empresa MGA."],
["Scene \"",2,[["Scene\"",753,true,false]],[[4,10]],""],["es una línea de",3,
[["is a line of",671,true,false],["is a range of",0,true,false],["is an online"
,0,true,false]],[[11,26]],""],["muñecas de",4,[["dolls from",537,true,false],
["dolls",369,true,false],["wrists",0,true,false],["doll",0,true,false]],[[27,37]],""]
,["la empresa Mattel",5,[["Mattel",588,true,false]],[[38,55]],""],["Barbie",6,[
["Barbie",473,true,false]],[[68,74]],""],["derivada de",7,[["derived from",231,true,false]
,["derived",233,true,false],["derivative",1,true,false],["derivative of",0,true,false]],
[[56,67]],""],["comercialmente",8,[["commercially",400,true,false],["commercial",0,true
,false]],[[97,111]],""],["lanzada",9,[["launched",303,true,false],["released",1,true,
false],["thrown",0,true,false],["was launched",0,true,false],["launch",0,true,false]]
,[[75,82]],""],["para competir",10,[["to compete",372,true,false],["compete",0,
true,false],["competing",0,true,false],["order to compete",0,true,false],["in order to 
compete",0,true,false]],[[83,96]],""],["con las",11,[["with",362,true,false],["the"
,0,true,false],["to",0,true,false],["to the",0,true,false]],[[112,119]],""],["Bratz"
,12,[["Bratz",360,true,false]],[[128,133]],""],["muñecas de la",13,[["dolls",155,true
,false]],[[120,127],[134,139]],""],["empresa",14,[["company",213,true,false],["business"
,3,true,false],["firm",1,true,false],["enterprise",0,true,false],["now",0,true,false]],
[[140,147]],""],["MGA",15,[["MGA",545,true,false],["AMS",2,true,false],["AGM",0,true,false]]
[[148,151]],""],[".",16,[[".",546,false,false]],[[151,152]],""]],,,[["es"]],11]


Sentences are retrieved from this output
*/

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.gtranslate.Language;
import com.gtranslate.Translator;



public class GoslateTranlsate {
	
	
	
	public static void main(String[] args) throws IOException {
		
		// creating an instance of modified google translate API
		// which is responsible for giving out multiple transaltions 
		//of the given sentnece
	
		Translator translate = Translator.getInstance();
		// Here we are reading the input from the input file
		// the name of the input file is passed as an argument

		File f = new File(args[0]);
		//creating a scanner object to read the  file
		Scanner scan = new Scanner(f);
		
		// Here we are creating an array of strings where we 
		//are storing the spanish sentences of a pair
		String[] spanishsent =null;

		// We create an arraylist to store all the sentences
		// of the spanish input file
		ArrayList<String> sentences = new ArrayList<String>();
		String str = null;
		
		// Loop through the file to store all the sentences are 
		// added into the arraylist
		while(scan.hasNextLine())
		{	
			str = scan.nextLine();
			sentences.add(str);
			
		}

		// Here we are writting the output onto a file in the tmp folder of the
		// system. Argument specifies the number of input file in the input 
		// folder, for which output file is generated
		String Filename= "/tmp/Inputformanynew"+args[1]+".txt";
		//System.out.println(Filename);
		// Creating an instance of the Filewrite so as to write output
		// onto a new file
		File file = new File(Filename);

		// To give the permissions to file onto which the data is written 
		file.setExecutable(true, false);
	    file.setReadable(true, false);
	    file.setWritable(true, false);
		FileWriter fw = new FileWriter(file);
		
		BufferedWriter bw = new BufferedWriter(fw);
		if(!file.exists())
		{
			file.createNewFile();
		}
		

		// loop to write every translations on anew line each time. Now
		// the output file has translations for every sentence in a new
		// line
	for(int j =0; j<sentences.size();j++)
		
	{	// pair of spanish sentences are split into two strings
		// they are split with a tab space

		spanishsent =sentences.get(j).split("\t");
		// Then we translate the spanish sentence using Google API
		// which give the output of multiple translations in JSON format
		// it is written onto a new line
		spanishsent[0] = translate.translateCompleteResult(spanishsent[0], Language.SPANISH, Language.ENGLISH);
		bw.write(spanishsent[0]);
		bw.write("\n");

		// translation for the second sentence in th pair which is stored in spanishsent[1]
		spanishsent[1] = translate.translateCompleteResult(spanishsent[1], Language.SPANISH, Language.ENGLISH);
		bw.write(spanishsent[1]);
		bw.write("\n");
		
	}
	// closing the scanner , file and BufferedReader object
	scan.close();
	bw.close();
	fw.close();
	}

}
