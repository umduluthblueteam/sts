#!/bin/bash
#Author: Viswanadh
#Filename: runit.sh
#Description: Sets up the environment for running our system 


export PORT_NUMBER='25000'
 
export NLTK_DATA=`pwd`

#Input directories
SUBTASK_1_INPUT_DIRECTORY_PATH=`pwd`"/"Inputs/final_test_data/SubTask1
SUBTASK_2_INPUT_DIRECTORY_PATH=`pwd`"/"Inputs/final_test_data/SubTask2
SUBTASK_3_INPUT_DIRECTORY_PATH=`pwd`"/"Inputs/final_test_data/SubTask3
#Output direcetory
SUBTASK_1_OUTPUT_DIRECTORY_PATH=`pwd`"/"Outputs/final_test_data/SubTask1
SUBTASK_2_OUTPUT_DIRECTORY_PATH=`pwd`"/"Outputs/final_test_data/SubTask2
SUBTASK_3_OUTPUT_DIRECTORY_PATH=`pwd`"/"Outputs/final_test_data/SubTask3

#Train
TRAINING_DATA_FILE=`pwd`"/"Train_data/images_headlines.total.train

SCORING_PROGRAM_SUBTASK_1_2=`pwd`"/"correlation-noconfidence.pl
 
SCORING_PROGRAM_SUBTASK_3=evalF1.pl

VALIDATION_PROGRAM_SUBTASK_3=wellformed.pl 

cd src/
echo "Training classifier"
python TrainChunkFeatures.py $TRAINING_DATA_FILE
echo "done"
#compile java programs
javac -cp "../GoogleTranslate/Jars/*" GoslateTranslate.java
javac -cp "../Opennlp/Jars/*" OpennlpTools.java

echo "Running Main program..."
python Main.py  $SUBTASK_1_INPUT_DIRECTORY_PATH  \
				$SUBTASK_2_INPUT_DIRECTORY_PATH  \
				$SUBTASK_3_INPUT_DIRECTORY_PATH  \
				$SUBTASK_1_OUTPUT_DIRECTORY_PATH \
				$SUBTASK_2_OUTPUT_DIRECTORY_PATH \
				$SUBTASK_3_OUTPUT_DIRECTORY_PATH \
				$SCORING_PROGRAM_SUBTASK_1_2 \
				$VALIDATION_PROGRAM_SUBTASK_3 \
				$SCORING_PROGRAM_SUBTASK_3

if [ "$?" == 0 ]; then
echo "Program successfully terminated."
else
echo "An error occured."
fi