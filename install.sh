#!/bin/bash

#INSTALL DEPENDENCIES
#Install NLTK
echo -ne "Installing NLTK..."
pip install -U nltk --user >>/dev/null
echo "done"
#Download stopwords data
echo -ne "Downloading NLTK data..."
python -m nltk.downloader stopwords -d `pwd` >>/dev/null
echo "done"
#Install sklearn 
echo -ne "Installing Scikit learn..."
pip install -U scikit-learn --user >>/dev/null
echo "done"
#Install microsoft translator
echo -ne "Installing Bing translator..."
git clone https://github.com/openlabs/Microsoft-Translator-Python-API.git \
																>>/dev/null
cd Microsoft-Translator-Python-API/
python setup.py install --user &>/dev/null
echo "done"
cd .. 